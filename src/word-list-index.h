/* word-list-index.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <libipuz/libipuz.h>
#include <json-glib/json-glib.h>

G_BEGIN_DECLS


#define MAX_WORD_LENGTH 30

typedef enum
{
  WORD_LIST_VISIBILITY_TEST = 0,
  WORD_LIST_VISIBILITY_PLAYER,
  WORD_LIST_VISIBILITY_EDITOR,
} WordListVisibility;

/* Each WordSection represents a bucket of words with the same number of
 * Unicode code points - one WordSection for all the 3-letter words, another
 * one for all the 4-letter words, etc.  Each word may need a different number
 * of bytes ("UNA" is three bytes, but "UÑA" is four bytes).  Those two words
 * would be in the same bucket, with word_len=3.  For each bucket we find the
 * word that requires the largest number of bytes and use that to compute the
 * stride from one word to the next one in the same bucket.  So if the
 * eight-letter bucket only has "SÉCURITÉ" and "SECONDES", it would have
 * stride=(10 + 2): the first word has a 10-byte encoding, plus 1 byte for the
 * priority before each word, plus 1 byte for the nul terminator for each word.
 */
typedef struct
{
  /* Number of code points for each word in this index bucket (3 for FOO and FÖÖ).
   * Set to -1 when word_list_index_get_section() is asked for a nonexistent section.
   */
  gint word_len;

  /* Number of bytes from one word to the next in this bucket.  Computed from the 
   * longest encoded word, plus 1 for the word's priority, plus 1 for the nul terminator.
   */
  guint stride;

  /* Number of words in this bucket */
  guint count;

  /* Offset in the file to the first word in this bucket */
  guint offset;
} WordListSection;

typedef struct
{
  gchar *display_name;
  guint word_count;

  IpuzCharset *charset;
  gchar *filterchar;
  gint threshold;
  WordListVisibility visibility;

  gint min_length;
  gint max_length;

  gsize letter_list_offset;
  gsize letter_index_offset;

  gsize enumerations_offset;

  gsize anagram_word_list_offset;
  gsize anagram_hash_index_offset;
  gsize anagram_hash_index_length;

  gsize frequency_table_offset;

  /* The definition of the word section */
  GArray *words;
} WordListIndex;


WordListIndex   *word_list_index_new           (void) G_GNUC_WARN_UNUSED_RESULT;
WordListIndex   *word_list_index_new_from_json (JsonNode      *node) G_GNUC_WARN_UNUSED_RESULT;
void             word_list_index_free          (WordListIndex *index);
WordListSection  word_list_index_get_section   (WordListIndex *index,
                                                gint           word_len);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(WordListIndex, word_list_index_free);


G_END_DECLS
