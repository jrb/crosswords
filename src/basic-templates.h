/* basic-templates.h
 *
 * Copyright 2022 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include "puzzle-stack.h"

G_BEGIN_DECLS


#define SMALL_SIZE    8
#define MEDIUM_SIZE  11
#define LARGE_SIZE   15
#define X_LARGE_SIZE 21

typedef enum
{
  BASIC_SIZE_SMALL,   /* @ 8x8 @ */
  BASIC_SIZE_MEDIUM,  /* @ 11x11 @ */
  BASIC_SIZE_LARGE,   /* @ 15x15 @ */
  BASIC_SIZE_X_LARGE, /* @ 21x21 @ */
  BASIC_SIZE_CUSTOM,  /* @ User defined @ */
} BasicSize;


#define BASIC_TYPE_TEMPLATE_ROW (basic_template_row_get_type())
G_DECLARE_FINAL_TYPE (BasicTemplateRow, basic_template_row, BASIC, TEMPLATE_ROW, GObject);


GListModel *basic_template_get_model           (IpuzPuzzleKind    kind,
                                                BasicSize         size);
GListModel *basic_template_create_custom_model (IpuzPuzzleKind    kind,
                                                guint             width,
                                                guint             height);
GdkPixbuf  *basic_template_row_get_pixbuf      (BasicTemplateRow *template_row);
IpuzPuzzle *basic_template_row_get_template    (BasicTemplateRow *template_row);


G_END_DECLS
