/* edit-window-controls.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* This file is a part of the EditWindow widget and is not a
 * standalone object. Instead, it is meant to encapsulate all code
 * related to PanelWidget flows in one separate file for convenience.
 *
 * BE CAREFUL TOUCING THIS FILE. IT IS A COMPLEXITY SINK
 */


/* This macro can be used to call a grid_state_ operation on a given
 * state. It makes the code more legible and limits the amount of
 * boilerplate needed.
 */
#define STATE_OP(self,domain,op, ...) \
  self->edit_state->domain##_state =  \
    grid_state_replace (self->edit_state->domain##_state, \
                        grid_state_##op (self->edit_state->domain##_state, ## __VA_ARGS__));

/* Macros for getting the state from an EditWindow.
 */
#define GRID_STATE(self)  (self->edit_state->grid_state)
#define CLUES_STATE(self) (self->edit_state->clues_state)
#define STYLE_STATE(self) (self->edit_state->style_state)


#include "crosswords-config.h"
#include "crosswords-stats.h"
#include "cell-preview.h"
#include "crosswords-misc.h"
#include "edit-autofill-details.h"
#include "edit-bars.h"
#include "edit-cell.h"
#include "edit-clue-suggestions.h"
#include "edit-clue-details.h"
#include "edit-clue-list.h"
#include "edit-grid.h"
#include "edit-grid-info.h"
#include "edit-histogram.h"
#include "edit-metadata.h"
#include "edit-preview-window.h"
#include "edit-shapebg-row.h"
#include "edit-state.h"
#include "edit-symmetry.h"
#include "edit-window.h"
#include "edit-window-private.h"
#include "edit-word-list.h"
#include "word-array-model.h"
#include "word-list.h"
#include "word-list-resources.h"
#include "word-solver-task.h"

static void     load_puzzle_kind                          (EditWindow        *self);

/* update_* functions. These are for propagating the state from a new
   state to all the widgets in the editor. They don't touch the
   current state . */
static void     update_all                                (EditWindow        *self);
static void     update_grid_cursor_moved                  (EditWindow        *self);
static void     update_clues_cursor_moved                 (EditWindow        *self);
static void     update_clues_selection_changed            (EditWindow        *self);
static void     update_style_cursor_moved                 (EditWindow        *self);
static gboolean update_solver_timeout                     (EditWindow        *self);
static void     update_word_lists                         (EditWindow        *self);

/* State change functions. These are called after a change to the
 * puzzle. They modify the internal state to represent the new
 * state. */
static void     pushed_puzzle_new                         (EditWindow        *self);
static void     pushed_grid_changed                       (EditWindow        *self);
static void     pushed_state_changed                      (EditWindow        *self);
static void     pushed_metadata_changed                   (EditWindow        *self);

/* Sync functions. We do a best-effort to keep a modicum of state
 * between the three tabs as a convenience */
static void     sync_from_grid_state                      (EditWindow        *self);
static void     sync_from_clues_state                     (EditWindow        *self);
static void     sync_from_style_state                     (EditWindow        *self);

/* Modes */
static void     switch_grid_to_selectable                 (EditWindow        *self);
static void     switch_grid_to_editable                   (EditWindow        *self);

/* Helper functions */
static void     cancel_autofill                           (EditWindow        *self);
static void     clear_selection_edit_grid                 (EditWindow        *self);

/* Callbacks from widget. These handle individual change requests from
 * a widget. They are responsible for pushing the change (if
 * necessary, and calling appropriate update functions. */

/* REMINDER: Keep this list of prototypes updated — it's useful to have
 * a full list of callbacks all in one place. */

static void     symmetry_widget_symmetry_changed_cb       (EditWindow        *self,
                                                           IpuzSymmetry       symmetry);
static void     main_grid_guess_cb                        (EditWindow        *self,
                                                           const gchar       *guess);
static void     main_grid_guess_at_cell_cb                (EditWindow        *self,
                                                           const gchar       *guess,
                                                           IpuzCellCoord     *coord);
static void     main_grid_do_command_cb                   (EditWindow        *self,
                                                           GridCmdKind        kind,
                                                           GridSelectionMode  mode);
static void     main_grid_copy_cb                         (EditWindow        *self);
static void     main_grid_paste_cb                        (EditWindow        *self,
                                                           const gchar       *paste);
static void     main_grid_cell_selected_cb                (EditWindow        *self,
                                                           IpuzCellCoord     *coord);
static void     main_grid_select_drag_start_cb            (EditWindow        *self,
                                                           IpuzCellCoord     *anchor_coord,
                                                           IpuzCellCoord     *new_coord,
                                                           GridSelectionMode  mode);
static void     main_grid_select_drag_update_cb           (EditWindow        *self,
                                                           IpuzCellCoord     *anchor_coord,
                                                           IpuzCellCoord     *new_coord,
                                                           GridSelectionMode  mode);
static void     main_grid_select_drag_end_cb              (EditWindow        *self,
                                                           IpuzCellCoord     *anchor_coord);
static void     cell_widget_cell_type_changed_cb          (EditWindow        *self,
                                                           IpuzCellType       cell_type);
static void     cell_widget_side_changed_cb               (EditWindow        *self,
                                                           IpuzStyleSides     side);
static void     cell_widget_initial_val_changed_cb        (EditWindow        *self,
                                                           gboolean           use_initial_val);
static void     word_list_widget_word_activated_cb        (EditWindow        *self,
                                                           IpuzClueDirection  direction,
                                                           const gchar       *word,
                                                           const gchar       *enumeration_src);
static void     edit_autofill_details_grid_selected_cb    (EditWindow        *self,
                                                           guint              index);
static void     edit_autofill_details_start_solve_cb      (EditWindow        *self,
                                                           WordArrayModel    *skip_list,
                                                           gint               max_count);
static void     edit_autofill_details_stop_solve_cb       (EditWindow        *self);
static void     edit_autofill_details_reset_cb            (EditWindow        *self);
static void     clue_list_widget_clue_selected_cb         (EditWindow        *self,
                                                           IpuzClueId        *clue_id);
static void     clue_details_widget_clue_changed_cb       (EditWindow        *self,
                                                           const gchar       *new_clue,
                                                           IpuzEnumeration   *new_enumeration);
static void     clue_details_widget_selection_changed_cb  (EditWindow        *self,
                                                           const gchar       *selection_text);
static void     clue_suggestions_filter_changed_cb        (EditWindow        *self,
                                                           const gchar       *new_filter);
static void     edit_metadata_metadata_changed_cb         (EditWindow        *self,
                                                           gchar             *property,
                                                           gchar             *value);
static void     edit_metadata_show_enumerations_changed_cb (EditWindow        *self,
                                                           gboolean           show_enumerations);


static void
update_compact_autofill_visibility (EditWindow *self)
{
  if (task_runner_get_state (self->edit_state->runner) == TASK_RUNNER_RUNNING ||
      ipuz_grid_get_guesses (IPUZ_GRID (GRID_STATE(self)->xword)))
    {
      gtk_revealer_set_transition_type (GTK_REVEALER (self->compact_autofill_revealer),
                                        GTK_REVEALER_TRANSITION_TYPE_NONE);
      gtk_revealer_set_reveal_child (GTK_REVEALER (self->compact_autofill_revealer),
                                     TRUE);
    }
  else if (! GRID_STATE_HAS_SELECTABLE (GRID_STATE (self)))

    {
      gtk_revealer_set_transition_type (GTK_REVEALER (self->compact_autofill_revealer),
                                        GTK_REVEALER_TRANSITION_TYPE_SLIDE_UP);
      gtk_revealer_set_reveal_child (GTK_REVEALER (self->compact_autofill_revealer),
                                     FALSE);
    }
}

static void
update_color_rows (EditWindow *self)
{
  IpuzCell *cell;
  IpuzStyle *style;
  const gchar *color_str = NULL;

  cell = ipuz_grid_get_cell (IPUZ_GRID (STYLE_STATE (self)->xword),
                             &STYLE_STATE (self)->cursor);
  g_assert (cell);

  style = ipuz_cell_get_style (cell);

  if (style)
    color_str = ipuz_style_get_text_color (style);
  if (color_str)
    {
      GdkRGBA color;

      if (gdk_rgba_parse (&color, color_str))
        g_object_set (self->style_foreground_color_row,
                      "current-color", &color,
                      NULL);
    }
  else
    {
      g_object_set (self->style_foreground_color_row,
                    "current-color", NULL,
                    NULL);
    }

  if (style)
    color_str = ipuz_style_get_bg_color (style);
  else
    color_str = NULL;
  if (color_str)
    {
      GdkRGBA color;

      if (gdk_rgba_parse (&color, color_str))
        g_object_set (self->style_background_color_row,
                      "current-color", &color,
                      NULL);
    }
  else
    g_object_set (self->style_background_color_row,
                  "current-color", NULL,
                  NULL);
}

static void
update_all (EditWindow *self)
{
  IpuzPuzzle *puzzle;

  START_TIMER;

  VALIDATE_EDIT_STATE (self->edit_state);
  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);

  /* recalculate the info for the puzzle */
  g_clear_object (&self->edit_state->info);
  self->edit_state->info = ipuz_puzzle_get_puzzle_info (puzzle);

  switch (self->edit_state->stage)
    {
    case EDIT_STAGE_GRID:
      update_compact_autofill_visibility (self);
      edit_grid_update (EDIT_GRID (self->main_grid), GRID_STATE (self));
      cell_preview_update_from_cursor (CELL_PREVIEW (self->grid_preview),
                                       GRID_STATE (self),
                                       self->edit_state->sidebar_logo_config);
      edit_cell_update (EDIT_CELL (self->cell_widget), GRID_STATE (self), self->edit_state->sidebar_logo_config);
      edit_grid_info_update (EDIT_GRID_INFO (self->edit_grid_widget), GRID_STATE (self), self->edit_state->info);
      edit_histogram_update (EDIT_HISTOGRAM (self->letter_histogram_widget), self->edit_state->info);
      edit_histogram_update (EDIT_HISTOGRAM (self->cluelen_histogram_widget), self->edit_state->info);

      if (GRID_STATE_HAS_SELECTABLE (GRID_STATE (self)))
        {
          gtk_stack_set_visible_child_name (GTK_STACK (self->grid_cell_stack), "autofill");
          edit_autofill_details_update (EDIT_AUTOFILL_DETAILS (self->autofill_details),
                                        GRID_STATE (self),
                                        self->edit_state->runner,
                                        self->resource);
        }
      else
        {
          gtk_stack_set_visible_child_name (GTK_STACK (self->grid_cell_stack), "word_list");
          edit_autofill_details_update (EDIT_AUTOFILL_DETAILS (self->autofill_details_compact),
                                        GRID_STATE (self),
                                        self->edit_state->runner,
                                        self->resource);
          edit_word_list_update (EDIT_WORD_LIST (self->word_list_widget),
                                 GRID_STATE (self),
                                 self->resource);
        }
      break;
    case EDIT_STAGE_CLUES:
      edit_grid_update (EDIT_GRID (self->main_grid), CLUES_STATE (self));
      edit_clue_details_update (EDIT_CLUE_DETAILS (self->clue_details_widget),
                                CLUES_STATE (self),
                                self->edit_state->sidebar_logo_config);
      edit_clue_list_update (EDIT_CLUE_LIST (self->clue_list_widget), CLUES_STATE (self));
      break;
    case EDIT_STAGE_STYLE:
      edit_grid_update (EDIT_GRID (self->main_grid), STYLE_STATE (self));
      edit_shapebg_row_update (EDIT_SHAPEBG_ROW (self->style_shapebg_row), STYLE_STATE (self));
      update_color_rows (self);
      cell_preview_update_from_cursor (CELL_PREVIEW (self->style_preview),
                                       STYLE_STATE (self),
                                       self->edit_state->sidebar_logo_config);

      break;
    case EDIT_STAGE_METADATA:
      edit_metadata_update (EDIT_METADATA (self->edit_metadata), puzzle);
      break;
    default:
      g_assert_not_reached ();
    }

  /* Preview window updates on every change */
  if (self->edit_state->preview_window)
    edit_preview_window_update (EDIT_PREVIEW_WINDOW (self->edit_state->preview_window),
                                self->edit_state->preview_state);

  PUSH_TIMER ("update_all()");
}


/* Sets the initial state of a window for the current puzzle
 * kind. This is called once before any user interaction. */
static void
load_puzzle_kind (EditWindow *self)
{
  IpuzPuzzle *puzzle;
  IpuzPuzzleKind kind;
  const gchar *grid_stage = "cell";

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  kind = ipuz_puzzle_get_puzzle_kind (puzzle);

  self->edit_state->symmetry_widget_enabled = FALSE;
  self->edit_state->letter_histogram_widget_enabled = FALSE;
  self->edit_state->cluelen_histogram_widget_enabled = FALSE;

  /* Figure out the default setup */
  switch (kind)
    {
    case IPUZ_PUZZLE_CROSSWORD:
    case IPUZ_PUZZLE_CRYPTIC:
    case IPUZ_PUZZLE_BARRED:
      self->edit_state->symmetry_widget_enabled = TRUE;
      self->edit_state->letter_histogram_widget_enabled = TRUE;
      self->edit_state->cluelen_histogram_widget_enabled = TRUE;

      break;
    case IPUZ_PUZZLE_ACROSTIC:
      grid_stage = "acrostic";
      break;
    default:
      g_assert_not_reached ();
    }

  /* set the initial stage and page */
  _edit_window_set_stage (self, EDIT_STAGE_GRID);
  adw_view_stack_set_visible_child_name (ADW_VIEW_STACK (self->grid_type_stack),
                                         grid_stage);

  if (self->edit_state->symmetry_widget_enabled)
    {
      IpuzSymmetry symmetry;

      symmetry = crosswords_quirks_get_symmetry (self->edit_state->quirks);
      edit_symmetry_set_symmetry (EDIT_SYMMETRY (self->symmetry_widget), symmetry, self->edit_state->sidebar_logo_config);
    }

  gtk_widget_set_visible (self->letter_histogram_group, self->edit_state->letter_histogram_widget_enabled);
  gtk_widget_set_visible (self->cluelen_histogram_group, self->edit_state->cluelen_histogram_widget_enabled);
}

static void
update_grid_cursor_moved (EditWindow *self)
{
  if (self->edit_state->stage != EDIT_STAGE_GRID)
    return;

  update_compact_autofill_visibility (self);
  cell_preview_update_from_cursor (CELL_PREVIEW (self->grid_preview),
                                   GRID_STATE (self),
                                   self->edit_state->sidebar_logo_config);
  edit_grid_update (EDIT_GRID (self->main_grid), GRID_STATE (self));
  /* This won't fire until we allow keyboard nav for selection */
  if (GRID_STATE_HAS_SELECTABLE (GRID_STATE (self)))
    {
      gtk_stack_set_visible_child_name (GTK_STACK (self->grid_cell_stack), "autofill");
      edit_autofill_details_update (EDIT_AUTOFILL_DETAILS (self->autofill_details),
                                    GRID_STATE (self),
                                    self->edit_state->runner,
                                    self->resource);
    }
  else
    {
      gtk_stack_set_visible_child_name (GTK_STACK (self->grid_cell_stack), "word_list");
      edit_autofill_details_update (EDIT_AUTOFILL_DETAILS (self->autofill_details_compact),
                                    GRID_STATE (self),
                                    self->edit_state->runner,
                                    self->resource);
      edit_word_list_update (EDIT_WORD_LIST (self->word_list_widget),
                             GRID_STATE (self),
                             self->resource);
    }
  edit_cell_update (EDIT_CELL (self->cell_widget), GRID_STATE (self), self->edit_state->sidebar_logo_config);
}

static void
update_clues_cursor_moved (EditWindow *self)
{
  if (self->edit_state->stage != EDIT_STAGE_CLUES)
    return;

  edit_grid_update (EDIT_GRID (self->main_grid), CLUES_STATE (self));
  edit_clue_details_update (EDIT_CLUE_DETAILS (self->clue_details_widget),
                            CLUES_STATE (self),
                            self->edit_state->sidebar_logo_config);
  edit_clue_list_update (EDIT_CLUE_LIST (self->clue_list_widget), CLUES_STATE (self));
}

static void
update_clues_selection_changed (EditWindow *self)
{
  IpuzPuzzle *puzzle;

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);

  edit_clue_suggestions_update (EDIT_CLUE_SUGGESTIONS (self->clue_suggestions),
                                self->resource,
                                ipuz_puzzle_get_charset (puzzle),
                                self->edit_state->clue_selection_text);
}

static void
clue_suggestions_filter_changed_cb (EditWindow  *self,
                                    const gchar *new_filter)

{
  g_clear_pointer (&self->edit_state->clue_selection_text, g_free);
  self->edit_state->clue_selection_text = g_strdup (new_filter);
  update_clues_selection_changed (self);
}


static void
update_style_cursor_moved (EditWindow *self)
{
  if (self->edit_state->stage != EDIT_STAGE_STYLE)
    return;

  cell_preview_update_from_cursor (CELL_PREVIEW (self->style_preview),
                                   STYLE_STATE (self),
                                   self->edit_state->sidebar_logo_config);
  edit_shapebg_row_update (EDIT_SHAPEBG_ROW (self->style_shapebg_row), STYLE_STATE (self));
  update_color_rows (self);
  edit_grid_update (EDIT_GRID (self->main_grid), STYLE_STATE (self));
}

static gboolean
update_solver_timeout (EditWindow *self)
{
  update_all (self);

  if (task_runner_get_state (self->edit_state->runner) == TASK_RUNNER_RUNNING)
    return TRUE;

  self->edit_state->solver_timeout = 0;
  return FALSE;
}

static void
update_word_lists (EditWindow *self)
{
  IpuzPuzzle *puzzle;

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);

  edit_autofill_details_update (EDIT_AUTOFILL_DETAILS (self->autofill_details),
                                GRID_STATE (self),
                                self->edit_state->runner,
                                self->resource);
  edit_autofill_details_update (EDIT_AUTOFILL_DETAILS (self->autofill_details_compact),
                                GRID_STATE (self),
                                self->edit_state->runner,
                                self->resource);
  edit_word_list_update (EDIT_WORD_LIST (self->word_list_widget),
                         GRID_STATE (self),
                         self->resource);
  edit_clue_suggestions_update (EDIT_CLUE_SUGGESTIONS (self->clue_suggestions),
                                self->resource,
                                ipuz_puzzle_get_charset (puzzle),
                                self->edit_state->clue_selection_text);
}

/* This is called when we clear the stack and push a new puzzle onto
 * it. */
static void
pushed_puzzle_new (EditWindow *self)
{
  IpuzPuzzle *puzzle;

  cancel_autofill (self);

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);

  g_clear_pointer (&self->edit_state, edit_state_free);
  self->edit_state = edit_state_new (puzzle);

  /* Update widgets */
  load_puzzle_kind (self);
  update_all (self);
}

static void
pushed_grid_changed (EditWindow *self)
{
  IpuzPuzzle *puzzle;

  cancel_autofill (self);
  clear_selection_edit_grid (self);

  /* Make sure both states reflect the new puzzle */
  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);

  STATE_OP (self, grid,  swap_xword, IPUZ_CROSSWORD (puzzle));
  STATE_OP (self, clues, swap_xword, IPUZ_CROSSWORD (puzzle));
  STATE_OP (self, style, swap_xword, IPUZ_CROSSWORD (puzzle));

  update_all (self);
}

static void
pushed_state_changed (EditWindow *self)
{
  cancel_autofill (self);
  clear_selection_edit_grid (self);

  edit_word_list_update (EDIT_WORD_LIST (self->word_list_widget),
                         GRID_STATE (self),
                         self->resource);
}

static void
pushed_metadata_changed (EditWindow *self)
{
  IpuzPuzzle *puzzle;

  cancel_autofill (self);
  clear_selection_edit_grid (self);

  /* Make sure both states reflect the new puzzle */
  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  edit_metadata_update (EDIT_METADATA (self->edit_metadata), puzzle);
}

/*
 * Sync mutators. These functions try to keep the cursors sane between
 * the three tabs, and have them mirror each other as best as
 * possible.
 */
static void
sync_from_grid_state (EditWindow *self)
{
  /* Update the preferred direction */
  if (GRID_STATE (self)->clue.direction != IPUZ_CLUE_DIRECTION_NONE)
    self->edit_state->preferred_direction = GRID_STATE (self)->clue.direction;

  STATE_OP (self, clues, sync_to_coord,
            GRID_STATE (self)->cursor,
            self->edit_state->preferred_direction);
  STATE_OP (self, style, select_none);
}

static void
sync_from_clues_state (EditWindow *self)
{
  /* Update the preferred direction */
  if (CLUES_STATE (self)->clue.direction != IPUZ_CLUE_DIRECTION_NONE)
    self->edit_state->preferred_direction = CLUES_STATE (self)->clue.direction;

  /* Kill the selection, if it exists */
  switch_grid_to_editable (self);
  STATE_OP (self, grid, sync_to_coord,
            CLUES_STATE (self)->cursor,
            self->edit_state->preferred_direction);
  STATE_OP (self, style, select_none);
}

static void
sync_from_style_state (EditWindow *self)
{
  /* Note: The style page has no way to select a clue, so does not
   * affect the preferred_direction */
  switch_grid_to_editable (self);
  STATE_OP (self, grid, sync_to_coord,
            STYLE_STATE (self)->cursor,
            self->edit_state->preferred_direction);
  STATE_OP (self, clues, sync_to_coord,
            STYLE_STATE (self)->cursor,
            self->edit_state->preferred_direction);
}

static void
switch_grid_to_selectable (EditWindow *self)
{
  STATE_OP (self, grid, change_behavior,
            grid_state_mode_to_behavior (GRID_STATE_SELECT) |
            GRID_BEHAVIOR_USE_CURSOR |
            GRID_BEHAVIOR_EDIT_CELLS);
}

static void
switch_grid_to_editable (EditWindow *self)
{
  STATE_OP (self, grid, change_mode, GRID_STATE_EDIT);
  edit_grid_unset_anchor (EDIT_GRID (self->main_grid));

}

/*
 * Helper functions
 *
 * These functions modify the state, but don't update widgets.
 */

/* cancel_autofill is called by the pushed_* class of functions. At
 * this point, all autofill changes should be committed, and we are
 * guaranteed that an update will happen later. It just has to clear
 * the autofill machinery.
 *
 * We don't support autofill pencil markings or operations persisting
 * past any puzzle changes at this time.
 */
static void
cancel_autofill (EditWindow *self)
{
  if (self->edit_state == NULL)
    return;

  g_clear_handle_id (&self->edit_state->solver_timeout, g_source_remove);
  task_runner_reset (self->edit_state->runner);

  STATE_OP (self, grid, set_guesses, NULL);
}


static void
clear_selection_edit_grid (EditWindow *self)
{
  if (GRID_STATE_HAS_SELECTABLE (GRID_STATE (self)))
    {
      switch_grid_to_editable (self);
      STATE_OP (self, grid, cell_selected, GRID_STATE (self)->anchor);
      sync_from_grid_state (self);
    }
}

/*
 * Callbacks
 */

static void
symmetry_widget_symmetry_changed_cb (EditWindow   *self,
                                     IpuzSymmetry  symmetry)
{
  crosswords_quirks_set_symmetry (self->edit_state->quirks,
                                  symmetry);
}

static void
toggle_selection_cell_type (EditWindow *self)
{
  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  STATE_OP (self, grid, select_toggle_block);

  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (GRID_STATE (self)->xword));
}

static void
edit_grid_guess_cb (EditWindow  *self,
                    const gchar *guess)
{

  VALIDATE_EDIT_STATE (self->edit_state);

  /* FIXME(space): We should have this be the inverse of backspace and
   * catch it at the cell level. For now, clear the cell. */
  if (guess == NULL || guess[0] == '\0')
    {
      main_grid_do_command_cb (self, GRID_CMD_KIND_DELETE, 0);
      return;
    }
  /* Special case: If the puzzle is a crossword and you press '.' or
   * '#' then we toggle the selection based on whether there are any
   * normal squares in the selection.
   */
  if (GRID_STATE_HAS_SELECTABLE (GRID_STATE (self)) &&
      strstr (GRID_STATE_BLOCK_CHARS, guess) &&
      (self->edit_state->puzzle_kind == IPUZ_PUZZLE_CROSSWORD ||
       self->edit_state->puzzle_kind == IPUZ_PUZZLE_CRYPTIC))
    {
      toggle_selection_cell_type (self);
      return;
    }

  if (GRID_STATE_HAS_SELECTABLE (GRID_STATE (self)))
    {
      /* NOTE: While from some sort of temporal flow perspective it makes
       * sense to undo the selection, from a UX perspective it feels like
       * nonsense. Clear the selection before saving the state.
       */
      switch_grid_to_editable (self);
      STATE_OP (self, grid, sync_to_coord,
                GRID_STATE (self)->cursor,
                self->edit_state->preferred_direction);
    }

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  STATE_OP (self, grid, guess, guess);

  sync_from_grid_state (self);
  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (GRID_STATE (self)->xword));
}

static void
edit_grid_guess_at_cell_cb (EditWindow    *self,
                            const gchar   *guess,
                            IpuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  STATE_OP (self, grid, guess_at_cell, guess, *coord);

  sync_from_grid_state (self);
  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (GRID_STATE (self)->xword));
}

static void
edit_grid_do_command_cb (EditWindow        *self,
                         GridCmdKind        kind,
                         GridSelectionMode  mode)
{
  gboolean changes;

  VALIDATE_EDIT_STATE (self->edit_state);

  /* Special case arrows. Drop the selection if it exists and do a
     normal move. */
  /* FIXME(keynav): Shift/ctrl click really should extend the slection
   * or cursor. Unfortunately, that's not easy as it has to persist
   * the shift in order to keep the anchor around. Ignore it for
   * now. */
  if (CMD_KIND_ARROW (kind) &&
      GRID_STATE_HAS_SELECTABLE (GRID_STATE (self)))
    {
      switch_grid_to_editable (self);
    }

  changes = grid_state_command_destructive (GRID_STATE (self), kind);
  if (changes)
    edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                              self->puzzle_stack,
                              TRUE);

  STATE_OP (self, grid, do_command, kind);

  sync_from_grid_state (self);

  if (changes)
    puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                              IPUZ_PUZZLE (GRID_STATE (self)->xword));
  else
    update_grid_cursor_moved (self);
}

static void
edit_grid_cell_selected_cb (EditWindow    *self,
                            IpuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  /* This will be true, if we are toggling or extending the
   * extension. Otherwise, we go back to edit mode */
  if (self->ignore_next_main_grid_click)
    {
      self->ignore_next_main_grid_click = FALSE;
      return;
    }

  /* This is a normal click. We force us to be in edit mode if we
   * aren't already */
  if (GRID_STATE_HAS_SELECTABLE (GRID_STATE (self)))
    {
      self->ignore_next_main_grid_select = TRUE;
      switch_grid_to_editable (self);
    }

  STATE_OP (self, grid, cell_selected, *coord);

  sync_from_grid_state (self);
  update_grid_cursor_moved (self);
}

static void
edit_grid_copy_cb (EditWindow *self)
{
  GdkClipboard *clipboard;
  g_autofree gchar *str = NULL;

  /* If direction is NONE, we're probably on a block */
  if (GRID_STATE (self)->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    return;

  str = ipuz_clues_get_clue_string_by_id (IPUZ_CLUES (GRID_STATE (self)->xword),
                                          &GRID_STATE (self)->clue);
  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_set_text (clipboard, str);
}

static void
edit_grid_paste_cb (EditWindow  *self,
                    const gchar *str)
{
  IpuzClueDirection direction;

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  direction = GRID_STATE (self)->clue.direction;
  if (direction == IPUZ_CLUE_DIRECTION_NONE)
    direction = self->edit_state->preferred_direction;

  STATE_OP (self, grid, guess_word,
            GRID_STATE (self)->cursor,
            direction, str);

  sync_from_grid_state (self);
  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (GRID_STATE (self)->xword));
}

static void
edit_clues_cell_selected_cb (EditWindow    *self,
                             IpuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  edit_clue_details_commit_changes (EDIT_CLUE_DETAILS (self->clue_details_widget));

  STATE_OP (self, clues, cell_selected, *coord);

  sync_from_clues_state (self);
  update_clues_cursor_moved (self);
}

static void
edit_clues_copy_cb (EditWindow *self)
{
  GdkClipboard *clipboard;
  g_autofree gchar *str = NULL;

  /* If direction is NONE, we're probably on a block */
  if (CLUES_STATE (self)->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    return;

  str = ipuz_clues_get_clue_string_by_id (IPUZ_CLUES (CLUES_STATE (self)->xword),
                                          &CLUES_STATE (self)->clue);
  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_set_text (clipboard, str);
}


static void
edit_clues_do_command_cb (EditWindow  *self,
                          GridCmdKind  kind)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  edit_clue_details_commit_changes (EDIT_CLUE_DETAILS (self->clue_details_widget));

  STATE_OP (self, clues, do_command, kind);

  sync_from_clues_state (self);
  update_clues_cursor_moved (self);
}

static void
edit_style_copy_cb (EditWindow *self)
{
  GdkClipboard *clipboard;
  g_autofree gchar *str = NULL;

  /* If direction is NONE, we're probably on a block */
  if (STYLE_STATE (self)->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    return;

  str = ipuz_clues_get_clue_string_by_id (IPUZ_CLUES (STYLE_STATE (self)->xword),
                                          &STYLE_STATE (self)->clue);
  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_set_text (clipboard, str);
}

static void
edit_style_do_command_cb (EditWindow        *self,
                          GridCmdKind        kind,
                          GridSelectionMode  mode)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  STATE_OP (self, style, do_command, kind);

  sync_from_style_state (self);

  update_style_cursor_moved (self);
}

static void
main_grid_guess_cb (EditWindow  *self,
                    const gchar *guess)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_guess_cb (self, guess);
}

static void
main_grid_guess_at_cell_cb (EditWindow    *self,
                            const gchar   *guess,
                            IpuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_guess_at_cell_cb (self, guess, coord);
}

static void
main_grid_do_command_cb (EditWindow        *self,
                         GridCmdKind        kind,
                         GridSelectionMode  mode)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_do_command_cb (self, kind, mode);
  else if (self->edit_state->stage == EDIT_STAGE_CLUES)
    edit_clues_do_command_cb (self, kind);
  else if (self->edit_state->stage == EDIT_STAGE_STYLE)
    edit_style_do_command_cb (self, kind, mode);
}

static void
main_grid_cell_selected_cb (EditWindow    *self,
                            IpuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_cell_selected_cb (self, coord);
  else if (self->edit_state->stage == EDIT_STAGE_CLUES)
    edit_clues_cell_selected_cb (self, coord);
}

static void
main_grid_select_drag_start_cb (EditWindow        *self,
                                IpuzCellCoord     *anchor_coord,
                                IpuzCellCoord     *new_coord,
                                GridSelectionMode  mode)
{
  /* This will do nothing if we're not in a selection mode */
  if (self->edit_state->stage == EDIT_STAGE_GRID)
    {
      STATE_OP (self, grid, select_drag_start,
                *anchor_coord, *new_coord, mode);
      /* If we're in a selection mode AND we're extending or toggling the
       * extension, we need to ignore the next click as it will clear the
       * mode */
      if (mode != GRID_SELECTION_SELECT &&
          GRID_STATE_HAS_SELECTABLE (GRID_STATE (self)))
        self->ignore_next_main_grid_click = TRUE;
    }
  else if (self->edit_state->stage == EDIT_STAGE_STYLE)
    {
      STATE_OP (self, style, select_drag_start,
                *anchor_coord, *new_coord, mode);
      /* If we're in a selection mode AND we're extending or toggling the
       * extension, we need to ignore the next click as it will clear the
       * mode */
      if (mode != GRID_SELECTION_SELECT &&
          GRID_STATE_HAS_SELECTABLE (STYLE_STATE (self)))
        self->ignore_next_main_grid_click = TRUE;
    }
  update_all (self);
}

static void
edit_grid_select_drag_update_cb (EditWindow        *self,
                                 IpuzCellCoord     *anchor_coord,
                                 IpuzCellCoord     *new_coord,
                                 GridSelectionMode  mode)
{
  /* Check to see if we need to switch from normal edit mode to the
   * selectable mode. We do that if we've dragged into a different
   * coordinate. */
  if (!(GRID_STATE_HAS_SELECTABLE (GRID_STATE (self)) ||
        (ipuz_cell_coord_equal (anchor_coord, new_coord))))
    {
      switch_grid_to_selectable (self);

      /* start a drag */
      STATE_OP (self, grid, select_drag_start,
                *anchor_coord, *anchor_coord, mode);
    }

  STATE_OP (self, grid, select_drag_update,
            *anchor_coord, *new_coord, mode);

  update_all (self);
}

static void
style_grid_select_drag_update_cb (EditWindow        *self,
                                  IpuzCellCoord     *anchor_coord,
                                  IpuzCellCoord     *new_coord,
                                  GridSelectionMode  mode)
{
  STATE_OP (self, style, select_drag_update,
            *anchor_coord, *new_coord, mode);

  update_all (self);
}

static void
main_grid_select_drag_update_cb (EditWindow        *self,
                                 IpuzCellCoord     *anchor_coord,
                                 IpuzCellCoord     *new_coord,
                                 GridSelectionMode  mode)
{
  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_select_drag_update_cb (self, anchor_coord, new_coord, mode);
  else if (self->edit_state->stage == EDIT_STAGE_STYLE)
    style_grid_select_drag_update_cb (self, anchor_coord, new_coord, mode);
}

static void
main_grid_select_drag_end_cb (EditWindow    *self,
                              IpuzCellCoord *anchor_coord)
{
  if (self->edit_state->stage == EDIT_STAGE_GRID)
    {
      if (self->ignore_next_main_grid_select)
        {
          self->ignore_next_main_grid_select = FALSE;
          edit_grid_unset_anchor (EDIT_GRID (self->main_grid));
          return;
        }

      STATE_OP (self, grid, select_drag_end,
                *anchor_coord);

      self->ignore_next_main_grid_click = FALSE;

      sync_from_grid_state (self);
      update_all (self);
    }
  else if (self->edit_state->stage == EDIT_STAGE_STYLE)
    {
      STATE_OP (self, style, select_drag_end,
                *anchor_coord);

      self->ignore_next_main_grid_click = FALSE;

      sync_from_style_state (self);
      update_all (self);
    }
}

static void
main_grid_copy_cb (EditWindow *self)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_copy_cb (self);
  else if (self->edit_state->stage == EDIT_STAGE_CLUES)
    edit_clues_copy_cb (self);
  else if (self->edit_state->stage == EDIT_STAGE_STYLE)
    edit_style_copy_cb (self);
}

static void
main_grid_paste_cb (EditWindow  *self,
                    const gchar *str)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_paste_cb (self, str);
}

static void
cell_widget_cell_type_changed_cb (EditWindow   *self,
                                  IpuzCellType  cell_type)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack, TRUE);

  STATE_OP (self, grid, set_cell_type,
            GRID_STATE (self)->cursor, cell_type);

  sync_from_grid_state (self);
  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (GRID_STATE (self)->xword));
}

static void
cell_widget_side_changed_cb (EditWindow     *self,
                             IpuzStyleSides  side)
{
  IpuzSymmetry symmetry;
  IpuzStyleSides new_sides = 0;

  g_assert (IPUZ_IS_BARRED (GRID_STATE (self)->xword));

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack, TRUE);

  symmetry = crosswords_quirks_get_symmetry (GRID_STATE (self)->quirks);

  new_sides =
    ipuz_barred_calculate_side_toggle (IPUZ_BARRED (GRID_STATE (self)->xword),
                                       &GRID_STATE (self)->cursor,
                                       side,
                                       symmetry);

  STATE_OP (self, grid, set_bars,
            GRID_STATE (self)->cursor, new_sides, TRUE);

  sync_from_grid_state (self);
  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (GRID_STATE (self)->xword));
}

static void
cell_widget_initial_val_changed_cb (EditWindow *self,
                                    gboolean    use_initial_val)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  STATE_OP(self, grid, set_initial_val, use_initial_val);

  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (GRID_STATE (self)->xword));
}

static void
word_list_widget_word_activated_cb (EditWindow        *self,
                                    IpuzClueDirection  direction,
                                    const gchar       *word,
                                    const gchar       *enumeration_src)
{
  IpuzClue *clue;
  IpuzCellCoord coord;

  g_autoptr (IpuzEnumeration) enumeration = NULL;

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  clue = ipuz_clues_find_clue_by_coord (IPUZ_CLUES (GRID_STATE (self)->xword),
                                        direction,
                                        &GRID_STATE (self)->cursor);

  /* Some sanity checking. May fail in raw mode in the future */
  g_assert (clue != NULL);

  /* Update the enumeration */
  if (enumeration_src)
    {
      enumeration = ipuz_enumeration_new (enumeration_src, IPUZ_VERBOSITY_STANDARD);
    }
  ipuz_clue_set_enumeration (clue, enumeration);
  ipuz_crossword_fix_enumerations (GRID_STATE (self)->xword);

  if (ipuz_clue_get_n_coords (clue) == 0)
    return;

  ipuz_clue_get_coord (clue, 0, &coord);

  STATE_OP (self, grid, guess_word,
            coord, direction, word);

  sync_from_grid_state (self);
  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (GRID_STATE (self)->xword));
}

static void
solver_finished_cb (EditWindow *self)
{
  update_all (self);
}

static void
edit_autofill_details_grid_selected_cb (EditWindow *self,
                                        guint       index)
{
  IpuzGuesses *solution;
  g_autoptr (IpuzGuesses) solution_copy = NULL;

  /* Make a copy of the solution */
  solution = (IpuzGuesses *) task_runner_get_result (self->edit_state->runner, index);
  if (solution)
    solution_copy = ipuz_guesses_copy (solution);

  STATE_OP (self, grid, set_guesses, solution_copy);

  update_grid_cursor_moved (self);
}

static void
edit_autofill_details_start_solve_cb (EditWindow     *self,
                                      WordArrayModel *skip_list,
                                      gint            max_count)
{
  g_autoptr (PuzzleTask) puzzle_task = NULL;

  if (self->edit_state->solver_timeout != 0)
    {
      /* We didn't stop the timer... ./~ */
      g_warning ("Failed to clear the previous timeout");
      g_clear_handle_id (&self->edit_state->solver_timeout, g_source_remove);
    }

  if (self->edit_state->solver_finished_handler == 0)
    self->edit_state->solver_finished_handler =
      g_signal_connect_swapped (self->edit_state->runner,
                                "finished",
                                G_CALLBACK (solver_finished_cb), self);


  puzzle_task = (PuzzleTask *)
    word_solver_task_new (GRID_STATE (self)->xword,
                          self->resource,
                          GRID_STATE (self)->selected_cells,
                          word_array_model_get_array (skip_list),
                          max_count, 50);

  task_runner_run (self->edit_state->runner, puzzle_task);
  self->edit_state->solver_timeout =
    g_timeout_add (1000, G_SOURCE_FUNC (update_solver_timeout), self);
  update_all (self);
}

static void
edit_autofill_details_stop_solve_cb (EditWindow *self)
{
  g_clear_handle_id (&self->edit_state->solver_timeout, g_source_remove);
  task_runner_cancel (self->edit_state->runner);
  clear_selection_edit_grid (self);
  update_all (self);
}

static void
edit_autofill_details_reset_cb (EditWindow *self)
{
  cancel_autofill (self);
  clear_selection_edit_grid (self);
  update_all (self);
}

static void
clue_details_widget_clue_changed_cb (EditWindow      *self,
                                     const gchar     *new_clue_text,
                                     IpuzEnumeration *new_enumeration)
{
  IpuzClue *clue;

  clue = ipuz_clues_get_clue_by_id (IPUZ_CLUES (CLUES_STATE (self)->xword),
                                    &CLUES_STATE (self)->clue);

  g_return_if_fail (clue != NULL);

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  ipuz_clue_set_clue_text (clue, new_clue_text);
  ipuz_clue_set_enumeration (clue, new_enumeration);

  sync_from_clues_state (self);
  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (CLUES_STATE (self)->xword));
}

static void
clue_details_widget_selection_changed_cb (EditWindow  *self,
                                          const gchar *selection_text)
{
  g_clear_pointer (&self->edit_state->clue_selection_text, g_free);
  self->edit_state->clue_selection_text = g_strdup (selection_text);
  update_clues_selection_changed (self);
}

static void
clue_list_widget_clue_selected_cb (EditWindow *self,
                                   IpuzClueId *clue_id)
{
  IpuzClue *clue;

  VALIDATE_EDIT_STATE (self->edit_state);
  g_assert (clue_id);

  edit_clue_details_commit_changes (EDIT_CLUE_DETAILS (self->clue_details_widget));

  clue = ipuz_clues_get_clue_by_id (IPUZ_CLUES (CLUES_STATE (self)->xword),
                                    clue_id);

  STATE_OP (self, clues, clue_selected, clue);

  sync_from_clues_state (self);
  update_clues_cursor_moved (self);
}

static void
style_shapebg_changed_cb (EditWindow     *self,
                          IpuzStyleShape  shapebg)
{
  IpuzCell *cell;
  IpuzStyle *style;

  VALIDATE_EDIT_STATE (self->edit_state);

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  STATE_OP (self, style, ensure_lone_style,
            STYLE_STATE (self)->cursor);

  cell = ipuz_grid_get_cell (IPUZ_GRID (STYLE_STATE (self)->xword),
                             &STYLE_STATE (self)->cursor);
  g_assert (cell);
  style = ipuz_cell_get_style (cell);
  ipuz_style_set_shapebg (style, shapebg);

  ipuz_crossword_fix_styles (STYLE_STATE (self)->xword);

  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (STYLE_STATE (self)->xword));
}

static void
style_foreground_color_changed_cb (EditWindow *self,
                                   GdkRGBA    *color)
{
  IpuzCell *cell;
  IpuzStyle *style;
  g_autofree gchar *color_str = NULL;

  VALIDATE_EDIT_STATE (self->edit_state);

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  color_str = gdk_rgba_to_string (color);

  STATE_OP (self, style, ensure_lone_style,
            STYLE_STATE (self)->cursor);

  cell = ipuz_grid_get_cell (IPUZ_GRID (STYLE_STATE (self)->xword),
                             &STYLE_STATE (self)->cursor);
  g_assert (cell);
  style = ipuz_cell_get_style (cell);
  ipuz_style_set_text_color (style, color_str);

  ipuz_crossword_fix_styles (STYLE_STATE (self)->xword);

  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (STYLE_STATE (self)->xword));
}

static void
style_background_color_changed_cb (EditWindow *self,
                                   GdkRGBA    *color)
{
  IpuzCell *cell;
  IpuzStyle *style;
  g_autofree gchar *color_str = NULL;

  VALIDATE_EDIT_STATE (self->edit_state);

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  color_str = gdk_rgba_to_string (color);

  STATE_OP (self, style, ensure_lone_style,
            STYLE_STATE (self)->cursor);

  cell = ipuz_grid_get_cell (IPUZ_GRID (STYLE_STATE (self)->xword),
                             &STYLE_STATE (self)->cursor);
  g_assert (cell);
  style = ipuz_cell_get_style (cell);
  ipuz_style_set_bg_color (style, color_str);

  ipuz_crossword_fix_styles (STYLE_STATE (self)->xword);

  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (STYLE_STATE (self)->xword));
}

static void
edit_metadata_metadata_changed_cb (EditWindow *self,
                                   gchar      *property,
                                   gchar      *value)
{
  IpuzPuzzle *puzzle;

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  g_object_set (puzzle,
                property, value,
                NULL);
  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_METADATA,
                            puzzle);
}

static void
edit_metadata_show_enumerations_changed_cb (EditWindow *self,
                                           gboolean    show_enumerations)
{
  IpuzPuzzle *puzzle;
  gboolean old_show_enumerations = FALSE;

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  g_object_get (puzzle,
                "show-enumerations", &show_enumerations,
                NULL);

  if (old_show_enumerations ^ show_enumerations)
    {
      edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                                self->puzzle_stack,
                                TRUE);
      g_object_set (puzzle,
                    "show-enumerations", show_enumerations,
                    NULL);
      ipuz_crossword_fix_enumerations (IPUZ_CROSSWORD (puzzle));
      /* We need force the grid to redraw itself, as enumerations show
       * up visibly there. */
      puzzle_stack_push_change (self->puzzle_stack,
                                STACK_CHANGE_GRID,
                                puzzle);
    }
}

/* Actions */
/* Some actions are better done from this file rather than the ones in
   edit-window-controls.c. They can call the update functions from
   here. */
void
_win_grid_actions_commit_pencil (EditWindow  *self,
                                 const gchar *action_name,
                                 GVariant    *param)
{
  gboolean state_changed = FALSE;

  g_assert (EDIT_IS_WINDOW (self));

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  STATE_OP (self, grid, commit_pencil, &state_changed);

  if (state_changed)
    puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                              IPUZ_PUZZLE (GRID_STATE (self)->xword));
  else
    edit_state_clear_current_stack (GTK_WIDGET (self), self->puzzle_stack, TRUE);
}

void
_win_grid_actions_select_all (EditWindow *self)
{
  if (self->edit_state->stage == EDIT_STAGE_GRID)
    {
      switch_grid_to_selectable (self);
      STATE_OP (self, grid, select_all);
      update_all (self);
    }
  else if (self->edit_state->stage == EDIT_STAGE_CLUES)
    {
      edit_clue_details_select_all (EDIT_CLUE_DETAILS (self->clue_details_widget));
    }
}

void
_edit_window_puzzle_changed_cb (EditWindow            *self,
                                PuzzleStackOperation   operation,
                                PuzzleStackChangeType  change_type,
                                PuzzleStack           *puzzle_stack)
{
  IpuzPuzzle *puzzle;

  puzzle = puzzle_stack_get_puzzle (puzzle_stack);

  if (puzzle == NULL)
    return;

  switch (operation)
    {
    case PUZZLE_STACK_OPERATION_NEW_FRAME:
      if (change_type == STACK_CHANGE_PUZZLE)
        pushed_puzzle_new (self);
      else if (change_type == STACK_CHANGE_GRID)
        pushed_grid_changed (self);
      else if (change_type == STACK_CHANGE_STATE)
        pushed_state_changed (self);
      else if (change_type == STACK_CHANGE_METADATA)
        pushed_metadata_changed (self);
      edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state, self->puzzle_stack, FALSE);
      break;
    case PUZZLE_STACK_OPERATION_UNDO:
      edit_state_restore_from_stack (self->edit_state, self->puzzle_stack, TRUE);
      update_all (self);
      break;
    case PUZZLE_STACK_OPERATION_REDO:
      edit_state_restore_from_stack (self->edit_state, self->puzzle_stack, FALSE);
      update_all (self);
      break;
    }
}

void
_edit_window_class_controls_init (GtkWidgetClass *widget_class)
{
  g_assert (EDIT_IS_WINDOW_CLASS (widget_class));

  gtk_widget_class_bind_template_callback (widget_class, main_grid_guess_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_guess_at_cell_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_do_command_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_cell_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_select_drag_start_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_select_drag_update_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_select_drag_end_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_copy_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_paste_cb);
  gtk_widget_class_bind_template_callback (widget_class, word_list_widget_word_activated_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_autofill_details_grid_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_autofill_details_start_solve_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_autofill_details_stop_solve_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_autofill_details_reset_cb);
  gtk_widget_class_bind_template_callback (widget_class, cell_widget_side_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, cell_widget_cell_type_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, cell_widget_initial_val_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, symmetry_widget_symmetry_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_details_widget_clue_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_details_widget_selection_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_suggestions_filter_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_list_widget_clue_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, style_shapebg_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, style_foreground_color_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, style_background_color_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_metadata_metadata_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_metadata_show_enumerations_changed_cb);
}

void
_edit_window_control_update_stage (EditWindow *self)
{
  g_assert (EDIT_IS_WINDOW (self));

  g_object_set (self->main_grid,
                "stage", self->edit_state->stage,
                NULL);

  /* Clear the selection in the widget between the GRID and STYLE
   * stages. It'll be confusing if the anchor updates — this will
   * unset it at least */
  clear_selection_edit_grid (self);

  update_all (self);
}

void
_edit_window_resource_changed (EditWindow *self)
{
  WordListResource *resource;

  resource = word_list_resources_get_default_resource (WORD_LIST_RESOURCES_DEFAULT);
  g_object_ref (resource);

  g_clear_object (&self->resource);
  self->resource = resource;

  update_word_lists (self);
}
