/* play-clue-row.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include "crosswords-enums.h"
#include "crosswords-misc.h"
#include "play-clue-row.h"
#include "clue-grid.h"


enum
{
  PROP_0,
  PROP_MODE,
  N_PROPS
};

enum
{
  CLUE_CELL_SELECTED,
  CLUE_CELL_GUESS,
  CLUE_CELL_GUESS_AT_CELL,
  CLUE_CELL_DO_COMMAND,
  N_SIGNALS
};

static GParamSpec *obj_props[N_PROPS] =  {NULL, };
static guint obj_signals[N_SIGNALS] = { 0 };

struct _PlayClueRow
{
  GtkWidget  parent_instance;
  IpuzClueId clue_id;
  PlayClueRowMode mode;

  /* Widgets */
  GtkWidget *clue_label;
  GtkWidget *num_label;
  GtkWidget *clue_grid;
};


static void play_clue_row_init         (PlayClueRow      *self);
static void play_clue_row_class_init   (PlayClueRowClass *klass);
static void play_clue_row_set_property (GObject          *object,
                                        guint             prop_id,
                                        const GValue     *value,
                                        GParamSpec       *pspec);
static void play_clue_row_get_property (GObject          *object,
                                        guint             prop_id,
                                        GValue           *value,
                                        GParamSpec       *pspec);
static void play_clue_row_dispose      (GObject          *object);
static void clue_cell_selected_cb      (ClueGrid         *clue_grid,
		                        guint             row,
			                guint             column,
			                PlayClueRow      *self);
static void clue_cell_guess_cb         (ClueGrid         *clue_grid,
		                        const gchar      *guess,
			                PlayClueRow      *self);
static void clue_cell_guess_at_cell_cb (ClueGrid         *clue_grid,
		                        const gchar      *guess,
					guint             row,
					guint             column,
			                PlayClueRow      *self);
static void clue_cell_do_command_cb    (ClueGrid         *clue_grid,
		                        GridCmdKind       kind,
					PlayClueRow      *self);


G_DEFINE_TYPE (PlayClueRow, play_clue_row, GTK_TYPE_WIDGET);


static void
play_clue_row_init (PlayClueRow *self)
{
  GtkLayoutManager *box_layout;

  gtk_widget_init_template (GTK_WIDGET (self));

  self->mode = PLAY_CLUE_ROW_PLAY;
  self->clue_id.direction = IPUZ_CLUE_DIRECTION_NONE;

  box_layout = gtk_widget_get_layout_manager (GTK_WIDGET (self));
  gtk_orientable_set_orientation (GTK_ORIENTABLE (box_layout), GTK_ORIENTATION_VERTICAL);

  g_signal_connect (G_OBJECT (self->clue_grid), "clue-cell-selected", G_CALLBACK (clue_cell_selected_cb), self);
  g_signal_connect (G_OBJECT (self->clue_grid), "clue-cell-guess", G_CALLBACK (clue_cell_guess_cb), self);
  g_signal_connect (G_OBJECT (self->clue_grid), "clue-cell-guess-at-cell", G_CALLBACK (clue_cell_guess_at_cell_cb), self);
  g_signal_connect (G_OBJECT (self->clue_grid), "clue-cell-do-command", G_CALLBACK (clue_cell_do_command_cb), self);
}

static void
play_clue_row_class_init (PlayClueRowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = play_clue_row_set_property;
  object_class->get_property = play_clue_row_get_property;
  object_class->dispose = play_clue_row_dispose;

  obj_signals [CLUE_CELL_SELECTED] =
    g_signal_new ("clue-cell-selected",
                  PLAY_TYPE_CLUE_ROW,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2,
                  G_TYPE_UINT, G_TYPE_UINT);

  obj_signals [CLUE_CELL_GUESS] =
    g_signal_new ("clue-cell-guess",
                  PLAY_TYPE_CLUE_ROW,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);

  obj_signals [CLUE_CELL_GUESS_AT_CELL] =
    g_signal_new ("clue-cell-guess-at-cell",
                  PLAY_TYPE_CLUE_ROW,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 3,
                  G_TYPE_STRING,
		  G_TYPE_UINT, G_TYPE_UINT);

  obj_signals [CLUE_CELL_DO_COMMAND] =
    g_signal_new ("clue-cell-do-command",
                  PLAY_TYPE_CLUE_ROW,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  GRID_TYPE_CMD_KIND);

  obj_props[PROP_MODE] = g_param_spec_enum ("mode",
                                            NULL, NULL,
                                            PLAY_TYPE_CLUE_ROW_MODE,
                                            PLAY_CLUE_ROW_PLAY,
                                            G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  gtk_widget_class_set_css_name (widget_class, "clue-row");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/play-clue-row.ui");
  gtk_widget_class_bind_template_child (widget_class, PlayClueRow, num_label);
  gtk_widget_class_bind_template_child (widget_class, PlayClueRow, clue_label);
  gtk_widget_class_bind_template_child (widget_class, PlayClueRow, clue_grid);
}

static void
play_clue_row_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  PlayClueRow *self = PLAY_CLUE_ROW (object);

  switch (prop_id)
    {
    case PROP_MODE:
      play_clue_row_set_mode (self, g_value_get_enum (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
play_clue_row_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  PlayClueRow *self = PLAY_CLUE_ROW (object);

  switch (prop_id)
    {
    case PROP_MODE:
      g_value_set_enum (value, self->mode);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
play_clue_row_dispose (GObject *object)
{
  GtkWidget *child;

  g_return_if_fail (object != NULL);


  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (play_clue_row_parent_class)->dispose (object);
}

static void
clue_cell_selected_cb (ClueGrid    *clue_grid,
		       guint        row,
		       guint        column,
		       PlayClueRow *self)
{
  g_signal_emit (self,
		 obj_signals[CLUE_CELL_SELECTED],
		 0,
		 row, column);
};

static void
clue_cell_guess_cb (ClueGrid    *clue_grid,
		    const gchar *guess,
		    PlayClueRow *self)
{
  g_signal_emit (self,
		 obj_signals[CLUE_CELL_GUESS],
		 0,
		 guess);
};

static void
clue_cell_guess_at_cell_cb (ClueGrid  *clue_grid,
		            const gchar *guess,
			    guint        row,
			    guint        column,
			    PlayClueRow *self)
{
  g_signal_emit (self,
		 obj_signals[CLUE_CELL_GUESS_AT_CELL],
		 0,
		 guess,
		 row, column);
};

static void
clue_cell_do_command_cb (ClueGrid    *clue_grid,
		         GridCmdKind  kind,
			 PlayClueRow *self)
{
  g_signal_emit (self,
		 obj_signals[CLUE_CELL_DO_COMMAND],
		 0,
		 kind);
};

/* Public Members */

GtkWidget *
play_clue_row_new (GtkSizeGroup *size_group)
{
  PlayClueRow *clue_row;

  g_return_val_if_fail (GTK_IS_SIZE_GROUP (size_group), NULL);

  clue_row = g_object_new (PLAY_TYPE_CLUE_ROW, NULL);
  gtk_size_group_add_widget (size_group, clue_row->num_label);

  return GTK_WIDGET (clue_row);
}

/* This will try to encapsulate all the variations of the different
 * types of clue rows */
void
play_clue_row_set_mode (PlayClueRow     *clue_row,
                        PlayClueRowMode  mode)
{
  g_return_if_fail (PLAY_IS_CLUE_ROW (clue_row));

  if (clue_row->mode == mode)
    return;

  clue_row->mode = mode;

  /* Reset the styles */
  gtk_widget_remove_css_class (clue_row->num_label, "clued");
  gtk_widget_remove_css_class (clue_row->num_label, "answershown");
  gtk_widget_remove_css_class (clue_row->num_label, "guessed");
  gtk_widget_remove_css_class (clue_row->clue_label, "guessed");
  gtk_widget_remove_css_class (GTK_WIDGET (clue_row), "guessed-error");
      gtk_widget_remove_css_class (GTK_WIDGET (clue_row), "standalone");

  switch (mode)
    {
    case PLAY_CLUE_ROW_PLAY:
      gtk_label_set_width_chars (GTK_LABEL (clue_row->clue_label), 30);
      gtk_widget_set_visible (clue_row->clue_grid, FALSE);
      break;
    case PLAY_CLUE_ROW_PLAY_ACROSTIC:
      gtk_label_set_width_chars (GTK_LABEL (clue_row->clue_label), 30);
      gtk_widget_set_visible (clue_row->clue_grid, TRUE);
      break;
    case PLAY_CLUE_ROW_PLAY_STANDALONE:
      gtk_label_set_width_chars (GTK_LABEL (clue_row->clue_label), -1);
      gtk_widget_add_css_class (GTK_WIDGET (clue_row), "standalone");
      gtk_widget_set_visible (clue_row->clue_grid, FALSE);
      break;
    case PLAY_CLUE_ROW_EDIT:
      gtk_label_set_width_chars (GTK_LABEL (clue_row->clue_label), -1);
      gtk_widget_set_visible (clue_row->clue_grid, FALSE);
      break;
    default:
      g_assert_not_reached ();
    }
}

void
play_clue_row_update (PlayClueRow *clue_row,
                      GridState   *state,
                      IpuzClue    *clue,
                      IpuzClueId   clue_id,
                      gboolean     show_enumerations,
                      ZoomLevel    zoom_level)
{
  g_autofree gchar *clue_string = NULL;
  g_autofree gchar *num_text = NULL;
  const gchar *clue_text;
  gboolean correct = FALSE;

  g_return_if_fail (PLAY_IS_CLUE_ROW (clue_row));
  g_return_if_fail (clue != NULL);

  clue_row->clue_id = clue_id;

  /* Set the clue number/label */

  /* Note, the space character at the end of the numbering
   * text. That's an EM SPACE (U+2003). That's so we can have
   * the space between clues and clue numbers honor the current
   * font. */
  if (ipuz_clue_get_number (clue) > 0)
    num_text = g_strdup_printf ("<b>%d</b> ", ipuz_clue_get_number (clue));
  else if (ipuz_clue_get_label (clue) != NULL)
    num_text = g_markup_printf_escaped ("<b>%s</b> ", ipuz_clue_get_label (clue));

  if (num_text)
    {
      set_zoom_level_css_class (clue_row->num_label, NULL, zoom_level);
      gtk_label_set_markup (GTK_LABEL (clue_row->num_label), num_text);
      gtk_widget_set_visible (clue_row->num_label, TRUE);
    }
  else
    {
      gtk_widget_set_visible (clue_row->num_label, FALSE);
    }

  /* The clue label */

  clue_text = ipuz_clue_get_clue_text (clue);

  /* If the clue_text is empty, we special case the display */
  if (clue_text == NULL || clue_text[0] == '\0')
    {
      /* This is what we display for an empty clue row */
      if (clue_row->mode == PLAY_CLUE_ROW_EDIT)
        {
          clue_string = ipuz_clues_get_clue_string_by_id (IPUZ_CLUES (state->xword), &clue_id);
        }
      else
        {
          /* No enumerations for a blank clue — it doesn't make sense */
          clue_string = g_strdup (_("<i>&lt;empty></i>"));
        }
    }
  else if (show_enumerations)
    {
      IpuzEnumeration *enumeration = NULL;

      enumeration = ipuz_clue_get_enumeration (clue);
      if (ipuz_enumeration_get_display (enumeration))
        clue_string = g_strdup_printf ("%s  (%s)",
                                       clue_text,
                                       ipuz_enumeration_get_display (enumeration));
    }
  else
    {
      clue_string = g_strdup (clue_text);
    }

  set_zoom_level_css_class (clue_row->clue_label, NULL, zoom_level);

  /* set the correct css on the labels */
  if (clue_row->mode == PLAY_CLUE_ROW_EDIT)
    {
      if (clue_text != NULL && clue_text[0] != '\0')
        {
          gtk_widget_add_css_class (clue_row->clue_label, "clued");
          gtk_widget_remove_css_class (clue_row->clue_label, "answershown");
        }
      else
        {
          gtk_widget_add_css_class (clue_row->clue_label, "answershown");
          gtk_widget_remove_css_class (clue_row->clue_label, "clued");
        }
    }
  else
    {
      if (ipuz_clues_clue_guessed (IPUZ_CLUES (state->xword), clue, &correct))
        {
          if (state->reveal_mode != GRID_REVEAL_NONE && !correct)
            gtk_widget_add_css_class (GTK_WIDGET (clue_row), "guessed-error");
          else
            gtk_widget_remove_css_class (GTK_WIDGET (clue_row), "guessed-error");

          gtk_widget_add_css_class (clue_row->num_label, "guessed");
          gtk_widget_add_css_class (clue_row->clue_label, "guessed");
        }
      else
        {
          gtk_widget_remove_css_class (clue_row->num_label, "guessed");
          gtk_widget_remove_css_class (clue_row->clue_label, "guessed");
          gtk_widget_remove_css_class (GTK_WIDGET (clue_row), "guessed-error");
        }
    }
  gtk_label_set_markup (GTK_LABEL (clue_row->clue_label), clue_string);

  /* Display the acrostic clue grid */
  if (IPUZ_IS_ACROSTIC (state->xword))
    {
      LayoutConfig layout_config;

      layout_config = layout_config_at_zoom_level (IPUZ_PUZZLE_ACROSTIC, zoom_level);
      gtk_widget_set_visible (clue_row->clue_grid, TRUE);
      clue_grid_set_clue_id (CLUE_GRID (clue_row->clue_grid), clue_id);
      clue_grid_update_state (CLUE_GRID (clue_row->clue_grid), state, clue, layout_config);
    }
  else
    {
      gtk_widget_set_visible (clue_row->clue_grid, FALSE);
    }
}

IpuzClueId
play_clue_row_get_clue_id (PlayClueRow *clue_row)
{
  IpuzClueId none = { .direction = IPUZ_CLUE_DIRECTION_NONE, .index = 0 };

  g_return_val_if_fail (PLAY_IS_CLUE_ROW (clue_row), none);

  return clue_row->clue_id;
}

GtkWidget *
play_clue_row_get_selected_grid (PlayClueRow *clue_row)
{
  g_return_val_if_fail (PLAY_IS_CLUE_ROW (clue_row), NULL);

  return clue_row->clue_grid;
};
