/* edit-paned.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: (GPL-3.0-or-later or LGPL-2.0-or-later)
 */

/* Includes code from gtkpaned.c for resize support.
 *
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include "edit-paned.h"
#include <adwaita.h>


/* Include a simple stub widget for the handle */
#define EDIT_TYPE_PANED_HANDLE (edit_paned_handle_get_type())
G_DECLARE_FINAL_TYPE (EditPanedHandle, edit_paned_handle, EDIT, PANED_HANDLE, GtkWidget);

struct _EditPanedHandle
{
  GtkWidget parent_instance;
};


enum
{
  PROP_0,
  PROP_SHOW_END_CHILD,
  N_PROPS
};

static GParamSpec *obj_props[N_PROPS] = {NULL, };
static GtkBuildableIface *parent_buildable_iface;


struct _EditPaned
{
  GtkWidget parent_instance;

  gboolean show_end_child;

  GtkWidget *grid;
  GtkWidget *grid_swindow;
  GtkWidget *handle;
  GtkWidget *end_child;

  gint handle_size;
  gint swindow_min_height;
  gint end_child_min_height;

  /* handle dragging */
  GtkGesture *pan_gesture;  /* Used for touch */
  GtkGesture *drag_gesture; /* Used for mice */
  gint handle_offset; /* y position of the handle */
  gint drag_pos;      /* y position within the handle that we dragged */
  gboolean panning;

  /* Animation */
  AdwAnimation *show_end_child_animation;
  gfloat end_child_anim_value;
  GtkAllocation grid_start_alloc;
  GtkAllocation grid_end_alloc;
};


static void               edit_paned_handle_init         (EditPanedHandle      *self);
static void               edit_paned_handle_class_init   (EditPanedHandleClass *klass);

static void               edit_paned_init                (EditPaned            *self);
static void               edit_paned_class_init          (EditPanedClass       *klass);
static void               edit_paned_set_property        (GObject              *object,
                                                          guint                 prop_id,
                                                          const GValue         *value,
                                                          GParamSpec           *pspec);
static void               edit_paned_get_property        (GObject              *object,
                                                          guint                 prop_id,
                                                          GValue               *value,
                                                          GParamSpec           *pspec);
static void               edit_paned_dispose             (GObject              *object);
static void               edit_paned_size_allocate       (GtkWidget            *widget,
                                                          int                   width,
                                                          int                   height,
                                                          int                   baseline);
static void               edit_paned_measure             (GtkWidget            *widget,
                                                          GtkOrientation        orientation,
                                                          int                   for_size,
                                                          int                  *minimum,
                                                          int                  *natural,
                                                          int                  *minimum_baseline,
                                                          int                  *natural_baseline);
static GtkSizeRequestMode edit_paned_get_request_mode    (GtkWidget            *widget);
static void               edit_paned_buildable_init      (GtkBuildableIface    *iface);
static void               edit_paned_buildable_add_child (GtkBuildable         *buildable,
                                                          GtkBuilder           *builder,
                                                          GObject              *object,
                                                          const char           *type);
static void               run_show_end_child_animation   (EditPaned            *self,
                                                          gboolean              target_show_end_child);
static void               gesture_drag_begin_cb          (GtkGestureDrag       *gesture,
                                                          double                start_x,
                                                          double                start_y,
                                                          EditPaned            *self);
static void               gesture_drag_update_cb         (GtkGestureDrag       *gesture,
                                                          double                offset_x,
                                                          double                offset_y,
                                                          EditPaned            *self);
static void               gesture_drag_end_cb            (GtkGestureDrag       *gesture,
                                                          double                offset_x,
                                                          double                offset_y,
                                                          EditPaned            *self);


G_DEFINE_TYPE (EditPanedHandle, edit_paned_handle, GTK_TYPE_WIDGET);
G_DEFINE_TYPE_WITH_CODE (EditPaned, edit_paned, GTK_TYPE_WIDGET,
                         G_IMPLEMENT_INTERFACE (GTK_TYPE_BUILDABLE,
                                                edit_paned_buildable_init));

/*
 * EditPanedHandle
 */
static void
edit_paned_handle_init (EditPanedHandle *self)
{
  gtk_widget_add_css_class (GTK_WIDGET (self), "wide");
  gtk_widget_set_cursor_from_name (GTK_WIDGET (self), "row-resize");
}

static void
edit_paned_handle_class_init (EditPanedHandleClass *klass)
{
  gtk_widget_class_set_css_name (GTK_WIDGET_CLASS (klass), "separator");
}

/*
 * EditPaned
 */

static void
connect_drag_gesture_signals (EditPaned  *self,
                              GtkGesture *gesture)
{
  g_signal_connect (gesture, "drag-begin",
                    G_CALLBACK (gesture_drag_begin_cb), self);
  g_signal_connect (gesture, "drag-update",
                    G_CALLBACK (gesture_drag_update_cb), self);
  g_signal_connect (gesture, "drag-end",
                    G_CALLBACK (gesture_drag_end_cb), self);
}

static void
edit_paned_init (EditPaned *self)
{
  GtkGesture *gesture;

  self->show_end_child = TRUE;
  self->end_child_anim_value = 1.0;
  self->handle_size = 6;
  self->handle_offset = 0;


  /* Touch gesture */
  gesture = gtk_gesture_pan_new (GTK_ORIENTATION_VERTICAL);
  connect_drag_gesture_signals (self, gesture);
  gtk_gesture_single_set_touch_only (GTK_GESTURE_SINGLE (gesture), TRUE);
  gtk_event_controller_set_propagation_phase (GTK_EVENT_CONTROLLER (gesture),
                                              GTK_PHASE_CAPTURE);
  gtk_widget_add_controller (GTK_WIDGET (self), GTK_EVENT_CONTROLLER (gesture));
  self->pan_gesture = gesture;

  /* Pointer gesture */
  gesture = gtk_gesture_drag_new ();
  gtk_event_controller_set_propagation_phase (GTK_EVENT_CONTROLLER (gesture),
                                              GTK_PHASE_CAPTURE);
  connect_drag_gesture_signals (self, gesture);
  gtk_widget_add_controller (GTK_WIDGET (self), GTK_EVENT_CONTROLLER (gesture));
  self->drag_gesture = gesture;

  self->handle = g_object_new (EDIT_TYPE_PANED_HANDLE,
                               "height-request", self->handle_size,
                               NULL);
  gtk_widget_set_parent (self->handle, GTK_WIDGET (self));
  gtk_widget_add_css_class (GTK_WIDGET (self), "vertical");
}

static void
edit_paned_class_init (EditPanedClass *klass)
{
  GObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = G_OBJECT_CLASS (klass);
  widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = edit_paned_set_property;
  object_class->get_property = edit_paned_get_property;
  object_class->dispose = edit_paned_dispose;
  widget_class->size_allocate = edit_paned_size_allocate;
  widget_class->measure = edit_paned_measure;
  widget_class->get_request_mode = edit_paned_get_request_mode;

  obj_props[PROP_SHOW_END_CHILD] =
    g_param_spec_boolean ("show-end-child",
                          NULL, NULL,
                          TRUE,
                          G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  gtk_widget_class_set_css_name (widget_class, "paned");
}

static void
edit_paned_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  EditPaned *self = EDIT_PANED (object);
  gboolean show_end_child;

  switch (prop_id)
    {
    case PROP_SHOW_END_CHILD:
      show_end_child = g_value_get_boolean (value);
      run_show_end_child_animation (self, show_end_child);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_paned_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  EditPaned *self = EDIT_PANED (object);

  switch (prop_id)
    {
    case PROP_SHOW_END_CHILD:
      g_value_set_boolean (value, self->show_end_child);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_paned_dispose (GObject *object)
{
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (edit_paned_parent_class)->dispose (object);
}

static void
edit_paned_size_allocate (GtkWidget *widget,
                          int        width,
                          int        height,
                          int        baseline)
{
  EditPaned *self;
  gint grid_width;
  gint grid_height;
  gint grid_min_height;
  gint grid_min_width;
  gint grid_nat_width;
  gint grid_nat_height;
  gint child_nat = 0;
  gint child_min_baseline = -1;
  gint child_nat_baseline = -1;
  gint swindow_height = 0;
  gint handle_size = 0;
  gint end_child_height = 0;
  GtkAllocation allocation = {
    .width = width
  };

  self = EDIT_PANED (widget);

  /* Phase 1. We need to figure out the vertical laypout for the
   * widgets. For that, only the swindow, and end_child need
   * measuring. That will tell us how much space we have to work with
   * to allocate between the two. */

  /* Scrolled window */
  gtk_widget_measure (self->grid_swindow, GTK_ORIENTATION_VERTICAL, -1,
                      &self->swindow_min_height, &child_nat,
                      &child_min_baseline, &child_nat_baseline);

  /* end_child */
  if (gtk_widget_should_layout (self->end_child) && self->show_end_child)
    {
      gtk_widget_measure (self->end_child, GTK_ORIENTATION_VERTICAL, width,
                          &self->end_child_min_height, &child_nat,
                          &child_min_baseline, &child_nat_baseline);
      handle_size = self->handle_size;
      end_child_height = self->end_child_min_height;

      /* We might have resized since handle_offset was last used. We
       * need to clamp it to valid values */
      self->handle_offset = CLAMP (self->handle_offset, 0,
                                   (height - (self->end_child_min_height + self->swindow_min_height + self->handle_size)));
      /* account for the panel being moved */
      end_child_height += self->handle_offset;
    }
  else
    {
      self->end_child_min_height = 0;
      handle_size = 0;
      end_child_height = 0;
    }

  /* Now see if we need to shrink it further */
  swindow_height = height - ((end_child_height * self->end_child_anim_value) + handle_size);

  /* Phase 2. Determine the best width for the scrolled_window for the
   * grid to be its best size. Since specifying a width of the
   * scrolled-window will change the height of the grid, we want to
   * pick the largest width that will fit in the size the swindow has
   * available. */

  /* grid minimum sizes */
  gtk_widget_measure (self->grid, GTK_ORIENTATION_HORIZONTAL, -1,
                      &grid_min_width, &grid_nat_width,
                      &child_min_baseline, &child_nat_baseline);
  gtk_widget_measure (self->grid, GTK_ORIENTATION_VERTICAL, -1,
                      &grid_min_height, &grid_nat_height,
                      &child_min_baseline, &child_nat_baseline);
  grid_width = grid_nat_width;
  grid_height = grid_nat_height;

  /* Do we have enough width for the grid? Due to the size of
   * end-widget, this will almost never be triggered */
  if (grid_nat_width > width)
    {
      gint unused;

      gtk_widget_measure (self->grid, GTK_ORIENTATION_VERTICAL, width,
                          &unused, &grid_height,
                          &child_min_baseline, &child_nat_baseline);
      gtk_widget_measure (self->grid, GTK_ORIENTATION_HORIZONTAL, grid_height,
                          &unused, &grid_width,
                          &child_min_baseline, &child_nat_baseline);
    }

  if (swindow_height < grid_height)
    {
      gint unused;

      gtk_widget_measure (self->grid, GTK_ORIENTATION_HORIZONTAL, swindow_height,
                          &unused, &grid_width,
                          &child_min_baseline, &child_nat_baseline);
      gtk_widget_measure (self->grid, GTK_ORIENTATION_VERTICAL, grid_width,
                          &unused, &grid_height,
                          &child_min_baseline, &child_nat_baseline);
    }

  /* Allocate the scrolled window */
  allocation.x = (width - grid_width) / 2;
  allocation.y = 0;
  allocation.width = grid_width;
  allocation.height = swindow_height;
  gtk_widget_size_allocate (self->grid_swindow, &allocation, -1);

  if (gtk_widget_should_layout (self->end_child) && self->show_end_child)
    {
      /* Allocate the handle */
      allocation.x = 0;
      allocation.y = allocation.y + allocation.height;
      allocation.width = width;
      allocation.height = handle_size;
      gtk_widget_size_allocate (self->handle, &allocation, -1);

      /* Allocate the end_child. This is simpler, as it gets all the space it asks for (at a minimum) */
      allocation.y = allocation.y + allocation.height;
      allocation.height = end_child_height;

      gtk_widget_size_allocate (self->end_child, &allocation, -1);
    }
}


/* Get the width */
static void
edit_paned_measure_horizontal (GtkWidget      *widget,
                               GtkOrientation  orientation,
                               int             for_size,
                               int            *minimum,
                               int            *natural,
                               int            *minimum_baseline,
                               int            *natural_baseline)
{
  EditPaned *self;
  gint child_min = 0;
  gint child_nat = 0;
  gint child_min_baseline = -1;
  gint child_nat_baseline = -1;

  self = EDIT_PANED (widget);

  *minimum = 0;
  *natural = 0;

  /* Measure and ignore the handle */
  gtk_widget_measure (self->handle, orientation, for_size,
                      &child_min, &child_nat,
                      &child_min_baseline, &child_nat_baseline);

  /* Measure and ignore the swindow */
  gtk_widget_measure (self->grid_swindow, orientation, for_size,
                      &child_min, &child_nat,
                      &child_min_baseline, &child_nat_baseline);

  /* The grid size is important */
  if (gtk_widget_should_layout (self->grid))
    gtk_widget_measure (self->grid, orientation, for_size,
                        &child_min, &child_nat,
                        &child_min_baseline, &child_nat_baseline);

  *minimum = child_min;
  *natural = child_nat;

  /* get the width of the end widget. It's probably bigger */
  if (gtk_widget_should_layout (self->end_child))
    gtk_widget_measure (self->end_child, orientation, for_size,
                        &child_min, &child_nat,
                        &child_min_baseline, &child_nat_baseline);

  *minimum = MAX (*minimum, child_min);
  *natural = MAX (*natural, child_nat);
}

/* Get the height */
static void
edit_paned_measure_vertical (GtkWidget      *widget,
                             GtkOrientation  orientation,
                             int             for_size,
                             int            *minimum,
                             int            *natural,
                             int            *minimum_baseline,
                             int            *natural_baseline)
{
  EditPaned *self;
  gint child_min = 0;
  gint child_nat = 0;
  gint child_min_baseline = -1;
  gint child_nat_baseline = -1;

  self = EDIT_PANED (widget);

  *minimum = 0;
  *natural = 0;

  /* Measure and ignore the handle */
  gtk_widget_measure (self->handle, orientation, for_size,
                      &child_min, &child_nat,
                      &child_min_baseline, &child_nat_baseline);

  /* Measure the swindow to get the minimum height */
  gtk_widget_measure (self->grid_swindow, orientation, for_size,
                      &child_min, &child_nat,
                      &child_min_baseline, &child_nat_baseline);
  *minimum += child_min;

  /* The grid size determines the natural size */
  if (gtk_widget_should_layout (self->grid))
    gtk_widget_measure (self->grid, orientation, for_size,
                        &child_min, &child_nat,
                        &child_min_baseline, &child_nat_baseline);
  *natural += child_nat;

  /* Include the handle */
  *minimum += self->handle_size;
  *natural += self->handle_size;

  /* measure the end widget */
  if (gtk_widget_should_layout (self->end_child))
    gtk_widget_measure (self->end_child, orientation, for_size,
                        &child_min, &child_nat,
                        &child_min_baseline, &child_nat_baseline);
  *minimum += child_min;
  *natural += child_nat;
}

static void
edit_paned_measure (GtkWidget      *widget,
                    GtkOrientation  orientation,
                    int             for_size,
                    int            *minimum,
                    int            *natural,
                    int            *minimum_baseline,
                    int            *natural_baseline)
{
  EditPaned *self;

  self = EDIT_PANED (widget);

  g_assert ( self->grid != NULL && self->end_child != NULL);

  if (orientation == GTK_ORIENTATION_HORIZONTAL)
    edit_paned_measure_horizontal (widget, orientation, for_size,
                                   minimum, natural,
                                   minimum_baseline,
                                   natural_baseline);
  else if (orientation == GTK_ORIENTATION_VERTICAL)
    edit_paned_measure_vertical (widget, orientation, for_size,
                                   minimum, natural,
                                   minimum_baseline,
                                   natural_baseline);
  //g_print ("measure (%s): minimum: %d natural: %d\n", orientation==GTK_ORIENTATION_HORIZONTAL?"horizontal":"vertical", *minimum, *natural);
}

static GtkSizeRequestMode
edit_paned_get_request_mode (GtkWidget *widget)
{
  return GTK_SIZE_REQUEST_CONSTANT_SIZE;
}

static void
edit_paned_buildable_init (GtkBuildableIface *iface)
{
  parent_buildable_iface = g_type_interface_peek_parent (iface);

  iface->add_child = edit_paned_buildable_add_child;
}


static void
set_grid_widget (EditPaned *self,
                 GtkWidget *grid)
{
  if (self->grid == grid)
    return;

  if (self->grid_swindow)
    g_clear_pointer (&self->grid_swindow, gtk_widget_unparent);
  self->grid = grid;

  if (grid)
    {
      GtkWidget *viewport;
      /* Put this in a scrolled window */
      self->grid_swindow =
        (GtkWidget *) g_object_new (GTK_TYPE_SCROLLED_WINDOW,
                                    "valign", GTK_ALIGN_FILL,
                                    "vexpand", TRUE,
                                    "vscrollbar-policy", GTK_POLICY_AUTOMATIC,
                                    "hscrollbar-policy", GTK_POLICY_NEVER,
                                    NULL);
      viewport = g_object_new (GTK_TYPE_VIEWPORT,
                               "vscroll-policy", GTK_SCROLL_NATURAL,
                               NULL);
      gtk_scrolled_window_set_child (GTK_SCROLLED_WINDOW (self->grid_swindow), viewport);
      gtk_viewport_set_child (GTK_VIEWPORT (viewport), self->grid);
      gtk_widget_set_parent (self->grid_swindow, GTK_WIDGET (self));
    }
}


static void
set_buildable_child (EditPaned *self,
                     GtkWidget *child,
                     GtkWidget **dest)
{
  if (*dest == child)
    return;

  if (*dest)
    gtk_widget_unparent (*dest);
  *dest = child;

  if (child)
    gtk_widget_set_parent (*dest, GTK_WIDGET (self));
}

static void
edit_paned_buildable_add_child (GtkBuildable *buildable,
                                GtkBuilder   *builder,
                                GObject      *object,
                                const char   *type)
{
  EditPaned *self;
  GtkWidget *child;

  self = EDIT_PANED (buildable);
  child = GTK_WIDGET (object);

  if (type && strcmp (type, "grid") == 0)
    set_grid_widget (self, child);
  else if (type && strcmp (type, "end") == 0)
    set_buildable_child (self, child, &self->end_child);
  else
    parent_buildable_iface->add_child (buildable, builder, object, type);
}


static void
show_end_child_animation_done_cb (AdwAnimation *animation,
                                  EditPaned    *self)
{
  gtk_widget_set_child_visible (self->handle, (self->end_child_anim_value > 0.0));
  gtk_widget_set_child_visible (self->end_child, (self->end_child_anim_value > 0.0));
}

static void
show_end_child_animation_cb (double   value,
                             gpointer user_data)
{
  EditPaned *self = user_data;

  g_assert ((value <= 1.0) && (value >= 0.0));

  if (self->end_child_anim_value != value)
    {
      self->end_child_anim_value = value;
      gtk_widget_queue_allocate (GTK_WIDGET (self));
    }
  g_clear_object (&self->show_end_child_animation);
}


static void
run_show_end_child_animation (EditPaned *self,
                              gboolean   target_show_end_child)
{
  AdwAnimationTarget *target;
  gdouble from, to;

  g_clear_pointer (&self->show_end_child_animation, g_object_unref);

  from = target_show_end_child? 0.0: 1.0;
  to = target_show_end_child? 1.0: 0.0;
  target = adw_callback_animation_target_new (show_end_child_animation_cb, self, NULL);

  /* Match the animation in adw-overlay-split-view */
  self->show_end_child_animation =
    adw_spring_animation_new (GTK_WIDGET (self), from, to,
                              adw_spring_params_new (1, 0.5, 500), target);
  g_signal_connect (self->show_end_child_animation, "done",
                    G_CALLBACK (show_end_child_animation_done_cb),
                    self);

  if (target_show_end_child)
    {
      gtk_widget_set_child_visible (self->handle, TRUE);
      gtk_widget_set_child_visible (self->end_child, TRUE);
    }
  adw_animation_play (self->show_end_child_animation);
}

static gboolean
initiates_touch_drag (EditPaned *self,
                      double     start_x,
                      double     start_y)
{
  int handle_size, handle_pos, drag_pos;
  graphene_rect_t handle_area;

#define TOUCH_EXTRA_AREA_WIDTH 50
  if (!gtk_widget_compute_bounds (self->handle, GTK_WIDGET (self), &handle_area))
    return FALSE;

  handle_pos = handle_area.origin.y;
  drag_pos = start_y;
  handle_size = handle_area.size.height;

  if (drag_pos < handle_pos - TOUCH_EXTRA_AREA_WIDTH ||
      drag_pos > handle_pos + handle_size + TOUCH_EXTRA_AREA_WIDTH)
    return FALSE;

#undef TOUCH_EXTRA_AREA_WIDTH

  return TRUE;
}

static void
gesture_drag_begin_cb (GtkGestureDrag *gesture,
                       double          start_x,
                       double          start_y,
                       EditPaned      *self)
{
  GdkEventSequence *sequence;
  graphene_rect_t handle_area;
  GdkEvent *event;
  GdkDevice *device;
  gboolean is_touch;

  sequence = gtk_gesture_single_get_current_sequence (GTK_GESTURE_SINGLE (gesture));
  event = gtk_gesture_get_last_event (GTK_GESTURE (gesture), sequence);
  device = gdk_event_get_device (event);
  self->panning = FALSE;

  is_touch = (gdk_event_get_event_type (event) == GDK_TOUCH_BEGIN ||
              gdk_device_get_source (device) == GDK_SOURCE_TOUCHSCREEN);

  if (! gtk_widget_compute_bounds (self->handle, GTK_WIDGET (self), &handle_area))
    return;

  if ((is_touch && GTK_GESTURE (gesture) == self->drag_gesture) ||
      (!is_touch && GTK_GESTURE (gesture) == self->pan_gesture))
    {
      gtk_gesture_set_state (GTK_GESTURE (gesture),
                             GTK_EVENT_SEQUENCE_DENIED);
      return;
    }

  if (graphene_rect_contains_point (&handle_area, &(graphene_point_t){start_x, start_y}) ||
      (is_touch && initiates_touch_drag (self, start_x, start_y)))
    {
      self->drag_pos = start_y - handle_area.origin.y;

      self->panning = TRUE;

      gtk_gesture_set_state (GTK_GESTURE (gesture),
                             GTK_EVENT_SEQUENCE_CLAIMED);
    }
  else
    {
      gtk_gesture_set_state (GTK_GESTURE (gesture),
                             GTK_EVENT_SEQUENCE_DENIED);
    }
}

/* This is in absolute positions in widget coords */
static void
gesture_drag_update_cb (GtkGestureDrag   *gesture,
                        double            offset_x,
                        double            offset_y,
                        EditPaned        *self)
{
  double start_x, start_y;
  gint height;
  gint handle_ypos;

  height = gtk_widget_get_height (GTK_WIDGET (self));
  gtk_gesture_drag_get_start_point (GTK_GESTURE_DRAG (gesture),
                                    &start_x, &start_y);

  handle_ypos = start_y + offset_y - self->drag_pos;
  self->handle_offset = (height - (self->end_child_min_height + self->handle_size)) - handle_ypos;
  self->handle_offset = CLAMP (self->handle_offset, 0, height - (self->end_child_min_height + self->swindow_min_height));

  gtk_widget_queue_allocate (GTK_WIDGET (self));
}

static void
gesture_drag_end_cb (GtkGestureDrag *gesture,
                     double          offset_x,
                     double          offset_y,
                     EditPaned      *self)
{
  if (!self->panning)
    gtk_gesture_set_state (GTK_GESTURE (gesture), GTK_EVENT_SEQUENCE_DENIED);

  self->panning = FALSE;
}

