/* crosswords-misc.h
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include "grid-layout.h"
#include "svg.h"

G_BEGIN_DECLS



#define PUZZLE_SET_PREFIX "/org/gnome/Crosswords/puzzle-set/"



/* GTK+-ish functions */
GdkPixbuf  *xwd_pixbuf_load_from_gresource (GResource    *resource,
                                            const gchar  *resource_path);
IpuzPuzzle *xwd_load_puzzle_from_resource  (GResource    *resource,
                                            const char   *resource_path,
                                            GError      **error);
GdkPixbuf  *xwd_thumbnail_puzzle           (IpuzPuzzle   *puzzle);
guint       utf8_n_clusters                (const gchar  *text);
gchar      *utf8_get_next_cluster          (const gchar  *text,
                                            const gchar **end);
void        set_zoom_level_css_class       (GtkWidget    *widget,
                                            const char   *css_prefix,
                                            ZoomLevel     zoom_level);

Coloring coloring_from_adwaita             (void);


G_END_DECLS
