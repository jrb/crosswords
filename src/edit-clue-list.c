/* edit-clue-list.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "crosswords-misc.h"
#include "edit-clue-list.h"
#include "edit-grid.h"
#include "play-clue-list.h"
#include <libipuz/libipuz.h>


enum
{
  CLUE_SELECTED,
  N_SIGNALS
};

static guint obj_signals[N_SIGNALS] =  {0, };


struct _EditClueList
{
  GtkWidget parent_instance;

  GtkWidget *primary_clues;
  GtkWidget *secondary_clues;
};


static void edit_clue_list_init         (EditClueList          *self);
static void edit_clue_list_class_init   (EditClueListClass     *klass);
static void edit_clue_list_dispose      (GObject               *object);
static void clue_selected_cb            (EditClueList          *self,
                                         IpuzClueDirection      direction,
                                         gint                   index,
                                         PlayClueList          *clue_list);


G_DEFINE_TYPE (EditClueList, edit_clue_list, GTK_TYPE_WIDGET);


static void
edit_clue_list_init (EditClueList *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
edit_clue_list_class_init (EditClueListClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_clue_list_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-clue-list.ui");

  gtk_widget_class_bind_template_child (widget_class, EditClueList, primary_clues);
  gtk_widget_class_bind_template_child (widget_class, EditClueList, secondary_clues);

  gtk_widget_class_bind_template_callback (widget_class, clue_selected_cb);


  obj_signals [CLUE_SELECTED] =
    g_signal_new ("clue-selected",
                  EDIT_TYPE_CLUE_LIST,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  IPUZ_TYPE_CLUE_ID);
  
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);
}

static void
edit_clue_list_dispose (GObject *object)
{
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (edit_clue_list_parent_class)->dispose (object);
}

static void
clue_selected_cb (EditClueList      *self,
                  IpuzClueDirection  direction,
                  gint               index,
                  PlayClueList      *clue_list)
{
  IpuzClueId clue_id = {
    .direction = direction,
    .index = (guint) index,
  };

  g_signal_emit (self, obj_signals [CLUE_SELECTED], 0, &clue_id);

  //edit_grid_set_cursor (EDIT_GRID (self->edit_grid), self->state->cursor, self->state->clue);
}

void
edit_clue_list_update (EditClueList *self,
                       GridState    *clues_state)
{
  play_clue_list_update (PLAY_CLUE_LIST (self->primary_clues), clues_state, ZOOM_NORMAL);
  play_clue_list_update (PLAY_CLUE_LIST (self->secondary_clues), clues_state, ZOOM_NORMAL);
}
