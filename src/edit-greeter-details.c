/* edit-greeter-details.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "edit-greeter-details.h"


enum {
  ACTIVATED,
  N_SIGNALS
};

static guint signals[N_SIGNALS] = { 0 };


G_DEFINE_INTERFACE (EditGreeterDetails, edit_greeter_details, GTK_TYPE_WIDGET);


static void
edit_greeter_details_default_init (EditGreeterDetailsInterface *iface)
{
  signals [ACTIVATED] =
    g_signal_new ("activated",
                  EDIT_TYPE_GREETER_DETAILS,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  g_object_interface_install_property
    (iface, g_param_spec_boolean ("valid", NULL, NULL,
                                  TRUE,
                                  G_PARAM_READWRITE|G_PARAM_EXPLICIT_NOTIFY));
}

/* Public Methods */

void
edit_greeter_details_activated (EditGreeterDetails *self)
{
  g_return_if_fail (EDIT_IS_GREETER_DETAILS (self));

  g_signal_emit (self, signals [ACTIVATED], 0);
}

IpuzPuzzle *
edit_greeter_details_get_puzzle (EditGreeterDetails *self)
{
  IpuzPuzzle *puzzle = NULL;
  g_autoptr (GSettings) settings = NULL;
  g_autofree char *author = NULL;
  g_autofree char *copyright = NULL;
  g_autofree char *url = NULL;

  g_return_val_if_fail (EDIT_IS_GREETER_DETAILS (self), NULL);

  if (EDIT_GREETER_DETAILS_GET_IFACE (self)->get_puzzle)
    puzzle = EDIT_GREETER_DETAILS_GET_IFACE (self)->get_puzzle (self);

  g_assert (puzzle);

  settings = g_settings_new ("org.gnome.Crosswords.Editor");

  author = g_settings_get_string (settings, "default-author");
  if (author && *author)
    ipuz_puzzle_set_author (puzzle, author);

  copyright = g_settings_get_string (settings, "default-copyright");
  if (copyright && *copyright)
    ipuz_puzzle_set_copyright (puzzle, copyright);

  url = g_settings_get_string (settings, "default-url");
  if (url && *url)
    ipuz_puzzle_set_url (puzzle, url);

  return puzzle;
}

gboolean
edit_greeter_details_get_valid  (EditGreeterDetails *self)
{
  gboolean valid = FALSE;

  g_return_val_if_fail (EDIT_IS_GREETER_DETAILS (self), FALSE);

  g_object_get (self, "valid", &valid, NULL);

  return valid;
}

void
edit_greeter_details_set_valid (EditGreeterDetails *self,
                                gboolean            valid)
{
  g_return_if_fail (EDIT_IS_GREETER_DETAILS (self));

  g_object_set (self, "valid", valid, NULL);
}
