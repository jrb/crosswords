/* test-utils.c
 *
 * Copyright 2021 Federico Mena
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include <glib.h>
#include <locale.h>
#include "crosswords-misc.h"
#include "crosswords-stats.h"
#include "gen-word-list.h"
#include "word-list.h"


#define TEST_RESOURCE "../word-lists/test.gresource"
#define BRODA_RESOURCE "../word-lists/broda.gresource"


/* Uncommenting this will enable the profile test. This test will run
 * word-list matches in an infinite loop and never complete. That can
 * be used along with a profiler to profile the word-list MATCH
 * operation. Currently, it's ~30-40µs per filter as per last attempt
 * at profiling.
 *
 * # profile_test: 0.000293
 * # profile_test: 0.000290
 * # profile_test: 0.000291
 * # profile_test: 0.000298
 * # profile_test: 0.000291
 * ...
 */
//#define PROFILE_TEST


static WordList *
get_test_word_list (void)
{
  g_autofree gchar *path = NULL;
  g_autoptr (WordListResource) resource = NULL;
  WordList *word_list = NULL;

  path = g_test_build_filename (G_TEST_BUILT, TEST_RESOURCE, NULL);
  resource = word_list_resource_new_from_file (path);

  word_list = g_object_new (WORD_TYPE_LIST,
                            "resource", resource,
                            NULL);

  return word_list;
}

#if 0
static void
dump_bytes (GBytes *bytes)
{
  GError *error = NULL;
  GSubprocess *sub = g_subprocess_new ((G_SUBPROCESS_FLAGS_STDIN_PIPE
                                        | G_SUBPROCESS_FLAGS_STDOUT_PIPE
                                        | G_SUBPROCESS_FLAGS_STDERR_MERGE),
                                       &error,
                                       "od",
                                       /* hex offsets */
                                       "-A",
                                       "x",
                                       /* hex bytes, 1 byte per integer, add printout */
                                       "-t",
                                       "x1z",
                                       /* no duplicate suppression */
                                       "-v",
                                       NULL);
  GBytes *stdout_buf = NULL;
  gpointer stdout_data;
  gsize stdout_size;

  if (!sub)
    {
      g_error ("Could not spawn od(1): %s", error->message);
      return; /* unreachable */
    }

  if (!g_subprocess_communicate (sub,
                                 bytes, /* stdin */
                                 NULL, /* cancellable */
                                 &stdout_buf,
                                 NULL, /* stderr_buf */
                                 &error))
    {
      g_error ("Error while running od(1): %s", error->message);
      return; /* unreachable */
    }

  stdout_data = g_bytes_unref_to_data (stdout_buf, &stdout_size);

  fwrite (stdout_data, 1, stdout_size, stdout);

  g_free (stdout_data);
  g_object_unref (sub);
}
#endif

static void
loads_default (void)
{
  WordList *wl = get_test_word_list ();
  word_list_set_filter (wl, "A??", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (wl), ==, 16);
  g_object_unref (wl);
}

#if 0

typedef struct {
  const char *word;
  glong priority;
} WordToAdd;

static WordList *
create_test_word_list (const WordToAdd *words,
                       gsize            num_words,
                       gint             min_length,
                       gint             max_length,
                       gint             threshold)
{
  GenWordList *gen = gen_word_list_new (min_length, max_length, 0, BRODA_SCORED);

  for (gsize i = 0; i < num_words; i++)
    {
      gen_word_list_add_test_word (gen, words[i].word, words[i].priority);
    }

  gen_word_list_sort (gen);
  gen_word_list_build_charset (gen);
  gen_word_list_anagram_table (gen);
  gen_word_list_calculate_offsets (gen);
  create_anagram_fragments (gen);

  GOutputStream *stream = g_memory_output_stream_new_resizable ();
  gen_word_list_write_to_stream (gen, "Test Wordlist", stream);
  g_output_stream_close (stream, NULL, NULL);
  gen_word_list_free (gen);

  GBytes *bytes = g_memory_output_stream_steal_as_bytes (G_MEMORY_OUTPUT_STREAM (stream));
  g_object_unref (stream);

  dump_bytes (bytes);

  WordList *word_list = word_list_new_from_bytes (bytes);
  g_bytes_unref (bytes);

  return word_list;
}

static void
ascii_roundtrip (void)
{
  const WordToAdd words[] = {
    { .word = "GHIJK", .priority = 3 },
    { .word = "DEF", .priority = 1 },
    { .word = "ABC", .priority = 2 },
  };
  WordList *word_list = create_test_word_list (words, G_N_ELEMENTS (words), 3, 5, 0);

  word_list_set_filter (word_list, "???", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 2);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "ABC");
  g_assert_cmpstr (word_list_get_word (word_list, 1), ==, "DEF");
  g_assert_cmpint (word_list_get_priority (word_list, 0), ==, 2);
  g_assert_cmpint (word_list_get_priority (word_list, 1), ==, 1);
  WordIndex abc;
  WordIndex def;
  g_assert_true (word_list_get_word_index (word_list, 0, &abc));
  g_assert_true (word_list_get_word_index (word_list, 1, &def));

  word_list_set_filter (word_list, "????", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 0);

  word_list_set_filter (word_list, "?????", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 1);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "GHIJK");
  g_assert_cmpint (word_list_get_priority (word_list, 0), ==, 3);
  WordIndex ghijk;
  g_assert_true (word_list_get_word_index (word_list, 0, &ghijk));

  g_assert_cmpstr (word_list_get_indexed_word (word_list, abc), ==, "ABC");
  g_assert_cmpstr (word_list_get_indexed_word (word_list, def), ==, "DEF");
  g_assert_cmpstr (word_list_get_indexed_word (word_list, ghijk), ==, "GHIJK");

  g_assert_cmpint (word_list_get_indexed_priority (word_list, abc), ==, 2);
  g_assert_cmpint (word_list_get_indexed_priority (word_list, def), ==, 1);
  g_assert_cmpint (word_list_get_indexed_priority (word_list, ghijk), ==, 3);

  g_object_unref (word_list);
}

static void
utf8_roundtrip (void)
{
  const WordToAdd words[] = {
    { .word = "AÑO", .priority = 1 },
    { .word = "ÉSE", .priority = 1 },

    { .word = "HOLA", .priority = 2 },
    { .word = "PÂTÉ", .priority = 2 },
    { .word = "ZULU", .priority = 2 },
  };
  WordList *word_list = create_test_word_list (words, G_N_ELEMENTS (words), 3, 5, 0);

  word_list_set_filter (word_list, "???", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 2);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "AÑO");
  g_assert_cmpstr (word_list_get_word (word_list, 1), ==, "ÉSE");
  g_assert_cmpint (word_list_get_priority (word_list, 0), ==, 1);
  g_assert_cmpint (word_list_get_priority (word_list, 1), ==, 1);
  WordIndex ano;
  WordIndex ese;
  g_assert_true (word_list_get_word_index (word_list, 0, &ano));
  g_assert_true (word_list_get_word_index (word_list, 1, &ese));

  g_assert_cmpstr (word_list_get_indexed_word (word_list, ano), ==, "AÑO");
  g_assert_cmpstr (word_list_get_indexed_word (word_list, ese), ==, "ÉSE");

  g_assert_cmpint (word_list_get_indexed_priority (word_list, ano), ==, 1);
  g_assert_cmpint (word_list_get_indexed_priority (word_list, ese), ==, 1);

  word_list_set_filter (word_list, "A?O", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 1);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "AÑO");

  word_list_set_filter (word_list, "??E", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 1);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "ÉSE");

  word_list_set_filter (word_list, "????", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 3);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "HOLA");
  g_assert_cmpstr (word_list_get_word (word_list, 1), ==, "PÂTÉ");
  g_assert_cmpstr (word_list_get_word (word_list, 2), ==, "ZULU");

  g_object_unref (word_list);
}

#endif

static void
anagram_test (void)
{
  WordList *wl = get_test_word_list ();

  word_list_set_filter (wl, "CAT", WORD_LIST_ANAGRAM);
  g_assert (word_list_get_n_items (wl) == 4);

  g_assert (!g_strcmp0 (word_list_get_word (wl, 0), "ACT"));
  g_assert (!g_strcmp0 (word_list_get_word (wl, 1), "CAT"));
  g_assert (!g_strcmp0 (word_list_get_word (wl, 2), "CTA"));
  g_assert (!g_strcmp0 (word_list_get_word (wl, 3), "TAC"));

  word_list_set_filter (wl, "NOTANANAGRAM", WORD_LIST_ANAGRAM);
  g_assert (word_list_get_n_items (wl) == 0);

  g_object_unref (wl);
}

//#define DEBUG_INTERSECTION
static void
intersection_test (WordList *wl,
                   const gchar *filter1,
                   guint        pos1,
                   const gchar *filter2,
                   guint        pos2,
                   const gchar *intersection_str)
{
  g_autoptr (IpuzCharset) intersecting_chars = NULL;
  g_autofree gchar *charset_str = NULL;
  g_autoptr (WordArray) word_array1 = NULL;
  g_autoptr (WordArray) word_array2 = NULL;

#ifdef DEBUG_INTERSECTION
  g_autoptr (GTimer) timer = NULL;

  timer = g_timer_new ();
  g_timer_start (timer);
#endif

  word_list_find_intersection (wl,
                               filter1, pos1,
                               filter2, pos2,
                               &intersecting_chars,
                               &word_array1,
                               &word_array2);

  if (intersecting_chars)
    charset_str = ipuz_charset_serialize (intersecting_chars);

#ifdef DEBUG_INTERSECTION
  g_timer_stop (timer);
  g_print ("Intersecting: %s (%u) with %s (%u)\n\tIntersection: %s\n\tTime Elapsed: %f\n",
           filter1, pos1,
           filter2, pos2,
           charset_str,
           g_timer_elapsed (timer, NULL));

  for (guint i = 0; i < word_array1->len; i++)
    {
      WordIndex word_index =
        word_array_index (word_array1, i);
      g_print ("%s\n", word_list_get_indexed_word (wl, word_index));
    }

  for (guint i = 0; i < word_array2->len; i++)
    {
      WordIndex word_index =
        word_array_index (word_array2, i);
      g_print ("%s\n", word_list_get_indexed_word (wl, word_index));
    }
#endif
  g_assert_cmpstr (intersection_str, ==, charset_str);

}

static void
find_intersection (void)
{
  g_autoptr (WordList) wl = get_test_word_list ();

  intersection_test (wl,
                     "???", 1,
                     NULL, 0,
                     "ABCDEFGHIJKLMNOPQRSTUVWXY");
  intersection_test (wl,
                     "D???", 1,
                     "E???Y", 2,
                     "EI");
  intersection_test (wl,
                     "E???Y", 2,
                     "D???", 1,
                     "EI");

  intersection_test (wl,
                     "???????", 2,
                     "??????????????", 13,
                     "ABCDEFGHIKLMNOPRSTUVWXYZ");
  intersection_test (wl,
                     "??????????????", 13,
                     "???????", 2,
                     "ABCDEFGHIKLMNOPRSTUVWXYZ");

  intersection_test (wl,
                     "A????E", 3,
                     "??????????", 4,
                     "AELRT");
}

#define EPSILON 0.001

static void
frequency_test (void)
{
  WordList *wl = get_test_word_list ();
  gfloat frequency;
  gunichar c;

  /* Make sure the first character loads and is what we expect */
  c = g_utf8_get_char ("A");
  frequency = word_list_get_char_frequency (wl, 2, 0, c);
  g_assert_true (ABS (frequency - 0.0909) < EPSILON);

  /* This should be 0 */
  c = g_utf8_get_char ("C");
  frequency = word_list_get_char_frequency (wl, 2, 0, c);
  g_assert_true (ABS (frequency - 0.0f) < EPSILON);

  /* Make sure we can read from the end of the list.  */
  c = g_utf8_get_char ("Z");
  frequency = word_list_get_char_frequency (wl, 21, 20, c);
  g_assert_true (ABS (frequency - 0.0f) < EPSILON);

  g_object_unref (wl);
}

#ifdef PROFILE_TEST
static void
profile_test (void)
{
  g_autofree gchar *path = NULL;
  g_autoptr (WordListResource) resource = NULL;
  WordList *word_list = NULL;

  path = g_test_build_filename (G_TEST_BUILT, BRODA_RESOURCE, NULL);
  resource = word_list_resource_new_from_file (path);

  word_list = g_object_new (WORD_TYPE_LIST,
                            "resource", resource,
                            NULL);

  while (TRUE)
    {
      START_TIMER;
      word_list_set_filter (word_list, "A??????", WORD_LIST_MATCH);
      word_list_set_filter (word_list, "?????S", WORD_LIST_MATCH);
      word_list_set_filter (word_list, "TR??????", WORD_LIST_MATCH);
      word_list_set_filter (word_list, "?S?", WORD_LIST_MATCH);
      word_list_set_filter (word_list, "???ING", WORD_LIST_MATCH);
      word_list_set_filter (word_list, "P??????????", WORD_LIST_MATCH);
      word_list_set_filter (word_list, "CLEARHEADEDNESS", WORD_LIST_MATCH);
      word_list_set_filter (word_list, "?A?E", WORD_LIST_MATCH);
      END_TIMER; 
   }
}

#endif

int
main (int argc, char **argv)
{

  /* This check is for Arch, which doesn't install with any locales by
   * default. As there's no en_US.UTF-8, collation is somewhat
   * arbitrary and can't be relied on for tests.
   *
   * Fixes bug #233
   */
  if (!setlocale(LC_ALL, "en_US.UTF-8"))
    g_error ("word-list-tests requires the en_US.UTF-8 locale to be installed on the host system.");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/word_list/loads_default", loads_default);

#if 0
  g_test_add_func ("/word_list/ascii_roundtrip", ascii_roundtrip);
  g_test_add_func ("/word_list/utf8_roundtrip", utf8_roundtrip);
#endif

  g_test_add_func ("/word_list/set_anagram", anagram_test);
  g_test_add_func ("/word_list/find_intersection", find_intersection);
  g_test_add_func ("/word_list/frequency_test", frequency_test);

#ifdef PROFILE_TEST
  g_test_add_func ("/word_list/profile_test", profile_test);
#endif

  return g_test_run ();
}
