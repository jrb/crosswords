/* crosswords-stats.c
 *
 * Copyright 2025 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


// Assume 60 frames a second
#define ONE_FRAME (1.0/60.0)


#include "crosswords-stats.h"


static GHashTable *stats_table = NULL;

typedef struct
{
  guint count;
  guint count_over_frame;
  gdouble total_elapsed;
  gdouble max_elapsed;
} StatsEntry;

void
crosswords_stats_add (const gchar *domain,
                      gdouble      elapsed)
{
  StatsEntry *stats_entry;

  g_return_if_fail (domain != NULL);

  /* It's okay to leak the stats_entry. stats_table is never freed */
  if (stats_table == NULL)
    stats_table = g_hash_table_new (g_str_hash, g_str_equal);

  stats_entry = g_hash_table_lookup (stats_table, domain);
  if (stats_entry == NULL)
    {
      stats_entry = g_new0 (StatsEntry, 1);
      g_hash_table_insert (stats_table, (gpointer) domain, stats_entry);
    }

  stats_entry->count ++;
  if (elapsed > ONE_FRAME)
    stats_entry->count_over_frame ++;
  if (elapsed > stats_entry->max_elapsed)
    stats_entry->max_elapsed = elapsed;
  stats_entry->total_elapsed += elapsed;
}

void
crosswords_stats_print (void)
{
  GHashTableIter iter;
  gpointer key, value;

  if (stats_table == NULL)
    return;

  g_hash_table_iter_init (&iter, stats_table);
  while (g_hash_table_iter_next (&iter, &key, &value))
    {
      StatsEntry *stats_entry = value;
      g_print ("%s:\n", (const gchar *) key);

      g_print ("\tAverage time (%f µs)\n",
               stats_entry->total_elapsed / (gdouble) stats_entry->count);
      g_print ("\tLongest time (%f µs)\n", stats_entry->max_elapsed);
      g_print ("\tTotal iterations (%u)\n", stats_entry->count);
      if (stats_entry->count_over_frame > 0)
        g_print ("\tTotal iterations longer than one frame (%u)\n", stats_entry->count_over_frame);
      g_print ("\tTotal time elapsed (%f µs)\n\n", stats_entry->total_elapsed);
    }
}
