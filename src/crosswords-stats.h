/* crosswords-statistics.h
 *
 * Copyright 2025 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once


#include <glib.h>
#include "crosswords-config.h"


G_BEGIN_DECLS

/* Code for easily  profiling a function */
#ifdef DEVELOPMENT_BUILD
#define START_TIMER           g_autoptr(GTimer) _timer=g_timer_new(); g_timer_start(_timer);
#define CHECKPOINT_TIMER(str) g_print("%s(%s): %f\n",G_STRFUNC,str,g_timer_elapsed (_timer, NULL));
#define END_TIMER             g_timer_stop(_timer); g_print("%s: %f\n",G_STRFUNC,g_timer_elapsed (_timer, NULL));
#define PUSH_TIMER(str)       g_timer_stop(_timer); crosswords_stats_add(str, g_timer_elapsed (_timer,NULL));

#else /* DEVELOPMENT_BUILD */
#define START_TIMER           
#define CHECKPOINT_TIMER(str) 
#define END_TIMER             
#define PUSH_TIMER(str)       
#endif

void crosswords_stats_add   (const gchar *domain,
                             gdouble      elapsed);
void crosswords_stats_print (void);


G_END_DECLS

