/* edit-main.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include <glib-unix.h>
#include <gtk/gtk.h>
#include "edit-app.h"
#include "crosswords-config.h"
#include "crosswords-stats.h"


static void
print_devel_statistics (void)
{
#ifdef DEVELOPMENT_BUILD
  crosswords_stats_print ();
#endif
}

static gboolean
app_sigint_handler (gpointer app)
{
#ifdef DEVELOPMENT_BUILD
  /* This is a little goofy, but when typing ctrl-C on the terminal we
   * get the "^C" printed out before our stats. This will flush that
   * line so we can cut-and-paste easier.
   */
  g_print ("\n");
#endif
  g_application_quit (G_APPLICATION (app));

  return G_SOURCE_REMOVE;
}

static gboolean
app_sigusr1_handler (gpointer user_data)
{
  print_devel_statistics ();
  return G_SOURCE_CONTINUE;
}

int
main (int   argc,
      char *argv[])
{
  g_autoptr (EditApp) app = NULL;
  gint retval;

  app = edit_app_new ();

  g_unix_signal_add (SIGINT, app_sigint_handler, app);
  g_unix_signal_add (SIGUSR1, app_sigusr1_handler, NULL);

  retval = g_application_run (G_APPLICATION (app), argc, argv);

  print_devel_statistics ();

  return retval;
}

