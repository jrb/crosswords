
![GNOME Crosswords](data/images/hero.png)<br>
_A Crossword player and editor for GNOME._


**GNOME Crosswords** is a crossword player and editor.

Features:
*   Uses .ipuz files internally and supports a significant chunk of
    the open [.ipuz spec](http://ipuz.org/) for crosswords
*   Supports cryptic, acrostics, barred, arrowword, filippine, and
    rebus-style puzzles
*   Loads .ipuz, .puz, .jpz, and .xd files from disk
*   Supports standalone puzzle collections of crosswords with multiple
    ways of playing them
*   External puzzle set downloaders can be used to download puzzles
*   Extensive styling support for crosswords. Square, black and white
    crosswords are traditional, but this game can also take advantage
    of color and shapes
*   Reveal button to find mistakes in the puzzle
*   Hint button to suggest possible answers using [Peter Broda's Wordlist](https://peterbroda.me/crosswords/wordlist/?fbclid=IwAR04CeR_nhEW5M7CoK6Pyc3lxtzAlD9i9nk6pYadGXWtWN9pTBNWvHCE2hk)
*   Puzzle checksums for puzzles that don't include an answer
*   Respects the Desktop-wide dark-mode preference
*   Language-specific quirks
*   Adaptive sizing to let it work on tablets or mobile form-factors

The current version is 0.3.14. You can read about the latest changes in
the [NEWS](NEWS.md) file.

![Example Puzzle](data/images/a-dogs-day.png)<br>
_Example crossword_

### Installation

The game is available for most Linux distros at
[flathub](https://flathub.org/apps/details/org.gnome.Crosswords).

It is also available within Fedora: `sudo dnf install crosswords` will
install it.

![Example Puzzle](data/images/reveal.png)<br>
_Reveal button in action_

## GNOME Crossword Editor

This program was originally intended to be a standalone game. However,
it soon grew a separate crossword editor after we realized that having
one was needed to ship higher quality crosswords. It also became a
really interesting project to work on.

![Edit Greeter](data/images/edit-grid.png)<br>
_The Crossword Editor creating a grid_

The editor can be used to create basic crosswords with grids and
clues. It has a pattern solver and grid autofill dialog for filling in
hard-to-finish corners, and will make suggestions of words when
creating the grid. In addition, it has hints for clue writing.

![Edit Greeter](data/images/edit-clues.png)<br>
_The Crossword Editor editing clues_


## Crossword Puzzles
This game won't be fun at all without good puzzles! If you are
interested in joining me in building a great set of crosswords for
people to play, [let me know](mailto:jrb@gnome.org)! I'd love to add a
bunch more puzzles to the game and showcase other people's works.

We support standalone puzzle sets, so it should be possible to bundle
up a bunch of puzzles together under a common theme and distribute
them as a standalone.

We save crosswords as [ipuz](http://ipuz.org) files loaded by
[libipuz](https://gitlab.gnome.org/jrb/libipuz). This is a great, free
file format.

_ipuz is a trademark of Puzzazz, Inc., used with permission_

## Community

We have a budding community of folks working on the game, solver, and
puzzles on #crosswords on gimp matrix. Feel free to drop by and say
hi! We're happy to talk about anything crossword related.

You can join the chat room
[here](https://matrix.to/#/#crosswords:gnome.org).

> **IMPORTANT:** This project follows the [GNOME Code of
> Conduct](https://conduct.gnome.org/)

## Test it!

Bugs should be reported to the our
[bugtracking system](https://gitlab.gnome.org/jrb/crosswords/issues)
on GNOME gitlab.

## Development

There is a comprehensive [development
guide](https://jrb.pages.gitlab.gnome.org/crosswords/devel-docs/index.html)
with information on how the code fits together.

Hints for [building and running
crosswords](https://jrb.pages.gitlab.gnome.org/crosswords/devel-docs/build-and-run.html)
can be found within the development guild.
