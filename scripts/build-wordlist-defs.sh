#!/bin/sh

RAW_DATA="${MESON_SOURCE_ROOT}/word-lists/raw-wiktextract-data.jsonl"
if [ ! -f "$RAW_DATA" ]; then
    echo "Missing required file $RAW_DATA"
    echo
    echo "Download it from https://kaikki.org/dictionary/rawdata.html"
    exit 1
fi

cd "${MESON_SOURCE_ROOT}/tools/wiktionary-extractor/"

echo "Creating broda word list"

./def-extractor.py broda filtered-list
./def-extractor.py broda enums
./def-extractor.py broda gvariant-list

echo "Creating wordnik word list"

./def-extractor.py wordnik filtered-list
./def-extractor.py wordnik enums
./def-extractor.py wordnik gvariant-list

echo "Creating test word list"

./def-extractor.py test filtered-list
./def-extractor.py test enums
./def-extractor.py test gvariant-list

echo "Updated defs in ${MESON_SOURCE_ROOT}/word-lists/"
