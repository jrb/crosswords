# Release process checklist for Crosswords

Feel free to print this document or copy it to a text editor to check
off items while making a release. In general, it's good to start the
release process a week before the actual release.

## A week before beginning
- [ ] Refresh your memory with
      https://wiki.gnome.org/MaintainersCorner/Releasing
- [ ] Update the overview files and any other devel docs that need
      updating.
- [ ] Update README.md with any changes. Don't forget to update the version number!
- [ ] Rebuild with development off: `meson setup _build/ --reconfigure -Ddevelopment=false`
- [ ] Do some final testing:
- [ ] Go through outstanding issues! Make sure we're ready to go and
not forgetting something.
- [ ] Make sure we pass all tests! `ninja test -C _build`
- [ ] Run through manual tests
- [ ] Update screenshots in data/images/ with updated versions
- [ ] Update `NEWS`, see below for the preferred format and hints.
- [ ] Make sure the flatpak builds without network and the
      dependencies for the python and rust build are included.
    - [ ] Use `flatpak run --command=flatpak-builder-lint
          org.flatpak.Builder manifest org.gnome.Crossword.json` to
          validate the flatpak manifest. Do the editor too.
- [ ] Copy the `NEWS` into the metainfo files for appstream. Make sure
      to validate the result, and check that they shows up in the
      about dialog
    - [ ] Use `flatpak run --command=flatpak-builder-lint
          org.flatpak.Builder appstream _build/data/$APPSTREAMFILE` to
          validate the metainfo files.
- [ ] Update all devel docs. `emacs docs/*md`

## Translations
- [ ] Make sure all source files are in `po/POTFILES`. This can be
      done with `ls data/*.in.in puzzle-sets/{add*,uri}/*.in src/*{c,.ui}|cat|sort > po/POTFILES`
      results
- [ ] Update the po/pot files: `meson compile -C _build crosswords-update-po`
- [ ] Remind translators of upcoming release. The news strings will
      have changed
- [ ] File a bug to ask them to update translations

*Wait a week for translations to come in.*

## Update the word lists, if desired
- [ ] Instructions for that are at docs/word-entry.md

## Release the source
- [ ] Release libipuz first, if necessary. Update the requirement in
      meson.build as well.
- [ ] Update the date in both metainfo files!
- [ ] Increase the package version number in `meson.build` (it may
already be increased but not released; double-check it).
- [ ] Update org.gnome.Crosswords.Devel.json and test it
- [ ] Update org.gnome.Crosswords.Editor.Devel.json and test it
- [ ] Commit the changes above.
- [ ] Build it one more time
- [ ] Create a release with `meson dist` in the _build directory. This
won't work if there are uncommitted changes.
- [ ] Create a signed tag: `git tag -s x.y.z` with the version number.
- [ ] `git push origin master`
- [ ] `git push origin x.y.z` the signed tag
- [ ] Create a [release in Gitlab](#gitlab-release).

## Gitlab release

- [ ] Go to https://gitlab.gnome.org/jrb/crosswords/-/releases and
click the **New release** button.
- [ ] Select the tag `x.y.z` you created as part of the release steps.
- [ ] If there is an associated milestone, select it too.
- [ ] Fill in the release title - `x.y.z - stable` or `x.y.z -
development`.
- [ ] Copy the release notes from NEWS.

## Update Flathub

* [ ] Update the manifest in the flathub repo to point to the new tag.
* [ ] Test the manifest locally: `flatpak-builder --force-clean
      _flatpak/ org.gnome.Crosswords.json`
* [ ] Push the new version to the beta branch upstream
* [ ] Make sure the new version builds at https://flathub.org/builds/#/
* [ ] Merge with master
* [ ] Update the puzzle-sets


## Brag a little
* [ ] Blog, toot, and promote on TWIG

----
## Maintaining requirements.txt and python3-requirements.json

When python requirements change, update requirements.txt to match. You
will also have to rebuild the requirements file for flatpaks.  Inside
the container image, you should run:

```shell
./ci/update-python-requirements
```

----

## Format for release notes in NEWS

The `NEWS` file contains the release notes.  Please use something
close to this format; it is not mandatory, but makes the formatting
consistent, and is what tooling expects elsewhere - also by writing
Markdown, you can just cut&paste it into a Gitlab release.  You can
skim bits of the NEWS file for examples on style and content.

New entries go at the **top** of the file.

An easy way to double check you got all the news is to run `git log
x.y.z..@ --oneline`, where `x.y.z` is the last release tag.

Example News

```markdown
=============
Version x.y.z
=============

Release date: Month xx, 20xx

Optional Summary

## Changes: Crosswords x.y.z
* Update 1
* Update 2
* Update 3

## Changes: Crossword Editor x.y.z
* Update 1

## Special thanks for this release:

- Any people that you want to highlight.
```
