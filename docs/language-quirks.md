# Crossword Quirks
**Status**: Implemented | 2023

## Problem Statement

One challenge we've run into is propagating behavior changes to
XwordState. That system tends to follow a fairly simple approach:

$$
state + event \rightarrow state'
$$

Most of the time, this naive approach works fine. However, there are
times we want to be able to have the app environment factor into the
calculation of the new state. Rather than `PlayState` having all the
logic of trying to determine the environment, we encapsulate these
settings into a `CrosswordsQuirks` object. This can yield a different
state than just the event alone.

$$
state + event + quirk \rightarrow state''
$$

As an example, Dutch has an "IJ" digraph that appears in puzzles as a
character pair. If a user types a "J" on a cell with an "I" in it
already, then it will add the "IJ" to the cell rather than replace the
cell as per normal.

## Assumptions

We have a quirk GObject that can be included with a puzzle state. It's
optional — no quirks means standard event behavior.

In addition, it's possible to have quirks based on the overall
environment of the crossword game, so a quirks object without an
associated crossword is possible.

## List of quirks

* **IJ_DIGRAPH:** Dutch support for IJ digraph
* **GUESS_ADVANCE:** User setting for how to advance the cursor after
  a guess
* **SYMMETRY:** Desired user setting for the symmetry of the
  puzzle. Used only by the editor
