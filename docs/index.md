# Development guide for Crosswords
----------------------------------

This is the development guide for *Crosswords* and *Crossword
Editor*. It will help you understand how the applications fit together
and how best to work with the codebase.

# Basic Information
-------------------

* Instructions on how to [build and run from source](build-and-run.md)
* Bugs can be reported via the [bugtracking
form](https://gitlab.gnome.org/jrb/crosswords/issues) on GNOME gitlab

The codebase is a little complex. Here are some guides to help
understand how it all fits together:

* **[*Crosswords* overview](overview-crosswords.md)** — guide to the main game
* **[*Crosswords Editor* overview](overview-crossword-editor.md)** — guide to the editor
* **[Shared code overview](overview-internals.md)** — guide to code shared between the two applications
* **[Naming disambiguation](naming-disambiguation.md)** — guide to understanding source code file names

# Coding style
--------------

Please keep a consistent coding style throughout the code base:

* [Style Guide](style-guide.md)
* TODO: Testing guide

# Designs
---------

The more complex sections of the code have their own documentation to
describe how they work. These started as design proposals, and
graduated to be designs once they were implemented.

**Crosswords:**
* [PlayXwordColumn](play-xword-column.md)
* [Puzzle Downloader](puzzle-downloader.md)

**Puzzle Sets:**
* [Puzzle Sets](puzzle-sets.md)
* [Puzzle Set Resource](puzzle-set-resource.md)
* [Puzzle Set Tags](puzzle-set-tags.md)

**Crossword Editor:**
* [Edit Autosolve Dialog](edit-autofill-dialog.md)
* [Acrostic Support](acrostic-support.md)
* [Word Entry](word-entry.md)
* [Word Solver](word-solver.md)

**Shared Code:**
* [Background shapes for cells](cell-shapebg.md)
* [Crossword Quirks](language-quirks.md)
* [Crossword State](xword-state.md)
* [CSS in Crosswords](css.md)
* [Grid State Modes](grid-state-modes.md)
* [Grid Layout](grid-layout.md)
* [Immutable XwordState](stateless.md)
* [libipuz Edit Interface](ipuz-edit-interface.md)
* [Overlays on the grid](overlays.md)
* [Play Grid Sizing](play-grid-sizing.md)
* [Play State Modes refactor](xword-state-modes.md)
* [The PuzzleStack](puzzle-stack.md)
* [Word List](word-list.md)

# Design proposals
------------------

We write a design document before we write non-trivial sections of
code. This is so we can discuss issues before coding, and have a
big-picture development history that's separate from the commit
messages. If you'd like to extend this project in a non-trivial way,
please submit a design doc MR before you begin coding.

**Drafts:**
* [Basic Types](basic-types.md)
* [Startup Actions](startup-actions.md)
* [ClueGrid widget](clue-grid.md)
* [Word Scores](word-scores.md)

**Deprecated:**
* [Crossword Editor Panel Widgets](panel-widgets.md)
* [Game State](game-state.md)
* [xword-state](xword-state.md)

# Packagers
-----------

* [Notes on packaging Crosswords](PACKAGING.md)

# Maintainers
-------------

* [Release process instructions](RELEASING.md)
* TODO: CI documentation

