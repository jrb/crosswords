# TODO

A non-comprehensive list of things we want to do. The [gitlab
issues](https://gitlab.gnome.org/jrb/crosswords/-/issues) are more
comprehensive.

## Player
* Reveal / give up button
* ..and always, add more puzzles

## Editor
* Greeter
    * Waay more templates, and better ones
* Editor Window
    * Finish preview dialog
* Grid
    * Grid "score"
    * Highlight crossing letters / better suggestions
    * Fix selection
    * Show 'errors' mode.
        * Sections that don't have a valid word for them
        * NATICK words?
        * Unches
    * Autofill:
        * Back up to the start to get more interesting puzzles
* Clues:
    * Rich text editor for clues
    * Indicator hint dialogs
* Style
    * Add a style editor, and make sure what we have works well

* Metadata
    * Define licenses in license tag
    * Clean up charset support

## Wordlist
* Consider a syntax like Qat
* Byte swap for big endian
